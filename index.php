<?php

// Include the main class, the rest will be automatically loaded
(@include_once ('vendor/autoload.php')) OR die("vendeor/autoload.php konnte nicht gefunden werden. Bitte installieren!");
(@include_once ('engine/config.php')) OR die("Config.php konnte nicht gefunden werden. Bitte installieren!");

// Klassen automatisch inkludieren, wenn erforderlich
spl_autoload_register(function($klasse){
  $datei = str_replace("_", "/", $klasse);
  if(file_exists(__DIR__."/engine/".$datei.".inc.php")){
    require_once __DIR__."/engine/".$datei.".inc.php";
  }
});

helper::sec_session_start();

// $_GET und $_POST zusammenfasen
$request = array_merge($_GET, $_POST);
// Controller erstellen
$controller = new controller($request);

// Inhalt der Webanwendung ausgeben.
echo $controller->display();
