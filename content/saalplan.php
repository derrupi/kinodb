<?php

if(!empty($_GET)){
  $seats = explode(",",$_GET['resSeats']);
  foreach($seats as $seat){
    echo $seat."<br />";
  }
}
?>
<!doctype html>
<html lang='de'>
	<head>
		<title>Kino DB</title>
		<meta charset='utf-8' />
		<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
		<![endif]-->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,600" rel="stylesheet">
		<link href='../css/style.css' rel='stylesheet' type='text/css' />
    <link href='../css/theme.min.css' rel='stylesheet' type='text/css' />
    <link href='../css/kinosaal.min.css' rel='stylesheet' type='text/css' />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
    jQuery(document).ready(function($) {

      var allSeats = new Array();
          $(document).on('click', '.seat', function () {
            var status = $(this)[0].checked;
            var sitzplatz = $(this).val();
              if(status==true){
                  var seat = $(this).val();
                  allSeats.push(seat);
                  var old = $('.sitze').html();
                  var neu = "<li id='li-"+seat+"'>"+seat+"</li>" + old;
                  $('.sitze').html(neu);
                  document.getElementById('resSeats').value = allSeats;
              } else {
                var sitz = "li-" + sitzplatz;
                var el = document.getElementById(sitz);
                let index = allSeats.indexOf(sitzplatz);
                  if (index > -1) {
                     allSeats.splice(index, 1);
                  }
                el.remove();
              }
          });

      });
    </script>
	</head>
	<body>
    <header>
      <div class='wrapper'>
        <div class='row'>
          <div id='logo' class='col-xs-12 col-sm-3'>
            <a href='index.html'><img src='../asset/png/logo_kinodb.png' title='Kino DB' alt='Logo Kino DB'/></a>
          </div>
          <div class='col-xs-12 col-sm-7'>
            <nav id='main'>
              <ul>
                <li><a href='#'>Home</a></li>
                <li><a href='#'>Filme</a></li>
                <li><a href='#'>Tickets</a></li>
                <li><a href='#'>Kontakt</a></li>
              </ul>
            </nav>
          </div>
          <div class='col-xs-12 col-sm-2'>
            <div id='account'>
              <ul>
                <li><a href='#'><img src='../asset/account/suche.png' title='Suche' alt='Suche'/></a></li>
                <li><a href='#'><img src='../asset/account/icon.png' title='Useraccount' alt='Useraccount'/></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div id='saalplan'>
      	<div class='wrapper'>

<?php

for($x = 0; $x <= 10; $x++){
?>
<div class='row'>
  <div class='singleSeat'>
      <p class='seatText'><?php echo $x; ?></p>
  </div>
<?php
  for ($i = 0; $i <= 15; $i++) {
?>
      			<div class='singleSeat'>
      					<label class="container">
      						<input class="seat" name="<?php echo $x."-".$i; ?>" value='<?php echo $x."-".$i; ?>' type="checkbox">
      						<span class="checkmark"><p class='seatText'><?php echo $i; ?></p></span>
      					</label>
      			</div>
<?php
  }
 echo "</div>";
}
?>
      		</div>
      	</div>

      	<div>
          <form method="get" action='#' name='reservation'>
      		<h3>Ausgewählte Sitze:</h3>
      		<p class='sitze'>
      		</p>
      	</div>
      </div>
      <input type='hidden' name='resSeats' id='resSeats' value=''/>
      <input type='submit' value='abschicken' />
      </form>

  <footer>
    <div class='wrapper'>
      <div class='row'>
        <div class='col-xs-12 col-sm-6'>
          <p><b>KinoDB </b>ist eine innovative Kinoplattform, bei der man Spaß am Ticket kaufen hat. Entstanden ist dieses Projekt im Rahmen der Diplomarbeit des JWE Lehrgangs 2018 des WIFI der Wirtschaftskammer Salzburg.</p>
          <address>
            <b>Andreas Ruprecht</b><br />
            Klessheimer Allee 59b/4<br />
            5020 Salzburg<br />
            <a href='tel:+436763921500'>+43 676 39 21 500</a><br />
            <a href='mailto:kinodb@ruprecht.cc?subject=Anfrage'>kinodb@ruprecht.cc</a><br />
          </address>
        </div>
        <div class='col-xs-12 col-sm-6'>
          <ul class='text-right'>
            <li><a href='#'>Impressum</a></li>
            <li><a href='#'>Kontakt</a></li>
            <li><a href='#'>Datenschutzhinweis</a></li>

          </ul>
        </div>
      </div>
    </div>

    <div class='dark-footer'>
      <div class='wrapper'>
        <div class='row'>
          <div class='col-xs-12 col-sm-6'>
            &copy; 2018 | Andreas Ruprecht
          </div>
          <div class='col-xs-12 col-sm-6 text-right'>
            <span class='icon-facebook2'></span>  <span class='icon-twitter'></span>  <span class='icon-youtube'></span>
          </div>
        </div>
      </div>
    </div>
  </footer>

</body>
</html>
