<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class user_sendmail {

  private $vorname;
  private $nachname;
  private $email;
  private $template;
  private $subject;
  private $code;

  public function __construct(array $infos){
    foreach($infos as $key => $item){
      $this->$key = $item;
    }
  }

  public function sendMail(){

    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = '101.hostinglogin.net';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'hello@andreasruprecht.at';                 // SMTP username
        $mail->Password = 'nNwrithF(@GBP[HvozA4pwdffCiZ3U';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('hello@andreasruprecht.at', 'KinoDB');
        $mail->addAddress($this->email, $this->vorname." ".$this->nachname);     // Add a recipient

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $this->subject;
        $mail->Body    = self::getTemplate($this->template);

        $mail->send();
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
  }

  public function getTemplate($template){
    $message = file_get_contents('theme/frontend/emails/'.$template.'.html');
    $message = str_replace('%vorname%', $this->vorname, $message);
    $message = str_replace('%email%', $this->email, $message);
    $message = str_replace('%code%', $this->code, $message);
    return $message;
  }
}
