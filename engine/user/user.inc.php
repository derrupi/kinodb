<?php

class user_user {

  private $userid;
  private $code;
  private $username;
  private $vorname;
  private $nachname;
  private $email;
  private $passwort;
  private $passwortValidate;
  private $errorLog;

  public function __construct(array $daten){
    foreach ($daten as $key => $value){
      $this->$key = $value;
    }
  }

  public function getUsername(){
    return $this->username;
  }

  static public function getEmail(){
    return $this->email;
  }

  public function getError(){
    return $this->errorLog;
  }

  static public function getUser($email){
    $db = db_datenbank::get_instanz();
    $email = $db->escape($email);

    $sql = "SELECT * FROM benutzer WHERE email = '".$email."'";

    $ergebnis = $db->query($sql);
    $ausgabe = array();
    $daten = mysqli_fetch_assoc($ergebnis);
    foreach ($daten as $key => $value){
      $ausgabe[$key] = $value;
    }
    return $ausgabe;

  }

  public function deleteUser(){
    $db = db_datenbank::get_instanz();
    $userid = $db->escape($this->userid);

    $sql = "DELETE FROM benutzer WHERE id = '{$userid}'";
    if($db->query($sql)){
      return true;
    } else {
      return $this->error("Der Benutzer konnte nicht gelöscht werden");
    }
  }

  public function addUser(){
    $db = db_datenbank::get_instanz();
    $daten = new daten_validieren();

    $ausgabe = array();
    if(!self::checkUserExist($this->email)){
      $ausgabe['vorname'] = $daten->input("vorname", $this->vorname, 1);
      $ausgabe['nachname'] = $daten->input("nachname", $this->nachname, 1);
      $ausgabe['email'] = $daten->inputEmail("e-mail", $this->email, 1);
      $ausgabe['passwort'] = $daten->inputPassword($this->passwort, $this->passwortValidate);

      if(!helper::multiKeyExists($ausgabe, "status")){
        $max = count($ausgabe);
        $i = 1;

        $sql = "INSERT INTO benutzer SET ";
        foreach($ausgabe as $key => $item){
          $sql .= $key." = '".$item."'";
          if ($i++ != $max) {
            $sql .= ", ";
          } else {
            $sql .= " ";
          }
        }

        if($db->query($sql)){
          $email = new user_sendmail(array(
            'vorname' => $ausgabe['vorname'],
            'nachname' => $ausgabe['nachname'],
            'email' => $ausgabe['email'],
            'template' => 'regConfirmation',
            'subject' => 'Deine Anmeldung war erfolgreich'
          ));
            $email->sendMail();

          unset($ausgabe);
          $ausgabe['success'] = $this->error("Der Benutzer wurde erfolgreich angelegt!");
        } else {
          $ausgabe['error'] = $this->error("Beim Anlegen des Benutzers ist ein Fehler aufgetreten!");
        }

      }
    } else {
      $ausgabe['error'] = $this->error("Die Emailadresse existiert bereits in unserem System!");
    }
    return $ausgabe;
  }

  static public function checkUserExist($email){
    $db = db_datenbank::get_instanz();
    $email = $db->escape($email);

      $sql = "SELECT id FROM benutzer WHERE email = '{$email}'";
      $ergebnis = $db->query($sql);

        if(mysqli_num_rows($ergebnis) >= 1){
          $row = mysqli_fetch_assoc($ergebnis);
          return true;
        } else {
          return false;
        }
  }

  public function login(){
        $db = db_datenbank::get_instanz();
        $ausgabe = array();

        $email = $db->escape($this->email);
        $passwort = $db->escape($this->passwort);

        $ausgabe['emailLogin'] = $email;

        if(empty($email)){
          $ausgabe[] = $this->error("Du musst eine gültige Emailadresse angeben!");
        } else {
          if(empty($passwort)){
            $ausgabe[] = $this->error("Das Feld Passwort darf nicht leer sein!");
          } else {

          $ergebnis = $db->query("SELECT * FROM benutzer WHERE email = '{$email}'");
          $zeile = mysqli_fetch_assoc($ergebnis);

          if(mysqli_num_rows($ergebnis) != 1){
              $ausgabe[] = $this->error("Passwort und/oder Email ist falsch");
          } else {
            // Eingegebener Benutzername existiert => Passwort überprüfen.
            if(self::checkbrute($zeile['id'])){
              //Benutzer ist gesperrt.
              $mail = new user_sendmail(array(
                  'vorname' => $zeile['vorname'],
                  'email' => $zeile['email'],
                  'template' => 'lockedUser',
                  'subject' => 'Dein Benutzer wurde gesperrt!'
                )
              );
              $mail->sendMail();

              $ausgabe[] = $this->error("Dein Benutzer wurde gesperrt!");
            } else {
              if(!password_verify($this->passwort, $zeile['passwort'])){
                $now = time();
                $db->query("INSERT INTO login_attempts (user_id, time) VALUES ({$zeile["id"]}, '{$now}')");
                $ausgabe[] = $this->error("Passwort und/oder Benutzer ist falsch");

              } else {
                // Passwort wurde richtig eingegeben -> Benutzer einloggen
                $user_id = $zeile['id'];
                // Hole den user-agent string des Benutzers.
                $user_browser = $_SERVER['HTTP_USER_AGENT'];
                // XSS-Schutz, denn eventuell wir der Wert gedruckt
                $user_id = preg_replace("/[^0-9]+/", "", $user_id);
                $_SESSION['user_id'] = $user_id;
                $_SESSION['benutzer'] = $email;

                // Letztes Login und Anzahl Logins in DB speichern
                $db->query("UPDATE benutzer SET
                  last_login = NOW(),
                  anzahl_logins = anzahl_logins + 1
                  WHERE id = '{$zeile["id"]}'
                ");

                // Login erfolgreich.
                $ausgabe[] = $this->error("Du wurdest erfolgreich eingeloggt!");
                }
              }
            }
          }
        }
        return $ausgabe;
      }

  private function checkbrute($user_id) {
      $db = db_datenbank::get_instanz();
      $now = time();

      // Alle Login-Versuche der letzten zwei Stunden werden gezählt.
      $valid_attempts = $now - (2 * 60 * 60);

      $sql = "SELECT time FROM login_attempts WHERE user_id = $user_id AND time > '$valid_attempts'";
      $row = $db->query($sql);
      $count = mysqli_num_rows($row);

      if ($count > 5) {
          return true;
      } else {
          return false;
      }
  }

  public function unlockUser(){
    $db = db_datenbank::get_instanz();
    $userid = $db->escape($this->userid);

    $sql = "DELETE FROM login_attempts WHERE user_id = '{$userid}'";
    $db->query($sql);

    $ergebnis = $db->query("SELECT * FROM benutzer WHERE id = '{$userid}'");
    while($user = mysqli_fetch_assoc($ergebnis)){
      $mail = new user_sendmail(array(
          'vorname' => $user['vorname'],
          'email' => $user['email'],
          'template' => 'unlockUser',
          'subject' => 'Dein Benutzer wurde entsperrt'
        )
      );
      $mail->sendMail();
    }
  }

  public function sendNewPassword(){
    $db = db_datenbank::get_instanz();
    $email = $db->escape($this->email);
    $userid = $db->escape($this->userid);
    $ausgabe = array();

    $sql = "SELECT * FROM benutzer WHERE id = '{$userid}' XOR email = '{$email}'";
    $ergebnis = $db->query($sql);

    $count = mysqli_num_rows($ergebnis);
    $code = helper::getRandomCode(15);

    $row = mysqli_fetch_assoc($ergebnis);
    $userid = $row['id'];

    if($count >= 1){
      $db->query("UPDATE benutzer_passwort SET active = '0' WHERE user = '{$userid}'"); // alle Links des Users auf inactive setzen

      $now = time();

      $sql = "INSERT INTO benutzer_passwort (user, code, timestamp) VALUES('{$userid}','{$code}','{$now}')"; // neuen Code eintragen
      $db->query($sql);

        $mail = new user_sendmail(array(
            'vorname' => $row['vorname'],
            'email' => $row['email'],
            'code' => $code,
            'template' => 'newPasswort',
            'subject' => 'Passwort zurücksetzen'
          )
        );
        $mail->sendMail();
      $ausgabe['success'] = $this->error("Bitte überprüfe deine Emails, wir haben dir einen Link zugeschickt");
    } else {
      $ausgabe['error'] = $this->error("Benutzer konnte nicht gefunden werden!");
    }
    return $ausgabe;
  }

  public function setNewPassword(){
    $db = db_datenbank::get_instanz();
    $this->passwort = $db->escape($this->passwort);
    $this->passwortValidate = $db->escape($this->passwortValidate);
    $code = $this->code;

    $ausgabe = array();
    $daten = new daten_validieren();
    $ausgabe['passwort'] = $daten->inputPassword($this->passwort, $this->passwortValidate);

    if(!helper::multiKeyExists($ausgabe, "status")){
      if(helper::checkNewPasswortLink($code)){
        $sql = "SELECT pass.user, cust.*
          FROM benutzer_passwort pass
          JOIN benutzer cust ON cust.id = pass.user
          WHERE pass.code = '{$code}'";
        $ergebnis = $db->query($sql);
        $row = mysqli_fetch_assoc($ergebnis);

        if(mysqli_num_rows($ergebnis) >= 1){
          $sql = "UPDATE benutzer SET passwort = '".$ausgabe['passwort']."' WHERE id = '".$row['id']."'";
          $sql2 = "UPDATE benutzer_passwort SET active = '0' WHERE code = '".$code."'";
          $db->query($sql);
          $db->query($sql2);
          $ausgabe['success'] = $this->error("Password wurde erfolgreich geändert!");
        } else {
          $ausgabe['error'] = $this->error("Dein Passwort-Link ist abgelaufen oder ungültigt!");
        }
      }
    }
    return $ausgabe;
  }

  static public function checkLogin(){
    if(empty($_SESSION['benutzer'])){
      return false;
    } else {
      return true;
    }

  }

  static public function checkAdmin($email){
    $db = db_datenbank::get_instanz();
    $email = $db->escape($email);

    $sql = "SELECT admin FROM benutzer WHERE email = '".$email."' AND admin = '1'";
    $ergebnis = $db->query($sql);
    $count = mysqli_num_rows($ergebnis);
    if($count >= 1){
      return true;
    } else {
      return false;
    }
  }

  private function error($fehlermeldung){
    return array(
      "status" => 0,
      "error" => $fehlermeldung
    );
  }

}
