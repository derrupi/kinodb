<?php

class backend_kunden {

  public function getCustomers() {
    $db = db_datenbank::get_instanz();

    $sql = "SELECT cust.*, (SELECT COUNT(*) FROM login_attempts WHERE user_id = cust.id) AS count
      FROM benutzer cust";

    $ergebnis = $db->query($sql);

    $ausgabe = array();
    while($row = mysqli_fetch_assoc($ergebnis)){
      $ausgabe[] = array(
        'id' => $row['id'],
        'vorname' => $row['vorname'],
        'nachname' => $row['nachname'],
        'email' => $row['email'],
        'lastLogin' => $row['last_login'],
        'gesperrt' => $row['count']
      );
    }
    return $ausgabe;
  }

}
