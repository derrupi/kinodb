<?php

class backend_settings{

  static public function getDefaultShowPrice(){
    $db = db_datenbank::get_instanz();

    $sql = "SELECT grundpreis FROM settings WHERE id = '1'";
    $ergebnis = $db->query($sql);
    $row = mysqli_fetch_assoc($ergebnis);

      return $row['grundpreis'];
  }

  public function getSettings(){
    $db = db_datenbank::get_instanz();

    $sql = "SELECT * FROM settings WHERE id = '1'";
    $ergebnis = $db->query($sql);

    while($row = mysqli_fetch_assoc($ergebnis)){
      $ausgabe[] = $row;
    }
    return $ausgabe[0];
  }

  public function safeSettings(array $daten){
    $db = db_datenbank::get_instanz();
    $daten = $db->escape($daten);

    $sql = "UPDATE settings ";
    foreach($daten as $key => $value){
      $sql .= "SET {$key} = '$value' ";
    }
    $sql .= "WHERE id = '1'";

    if($db->query($sql)){
      return true;
    } else {
      return false;
    }
  }
}
