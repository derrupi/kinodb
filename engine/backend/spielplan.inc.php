<?php

class backend_spielplan {

  public function getSpielplan() {

  $db = db_datenbank::get_instanz();
    $sql = "SELECT filme.*, laufzeit.startdatum, laufzeit.enddatum
      FROM filme
      INNER JOIN laufzeit ON laufzeit.film = filme.id ORDER BY laufzeit.startdatum";

  $ergebnis = $db->query($sql);

  $ausgabe = array();
  while($row = mysqli_fetch_assoc($ergebnis)){
    $ausgabe[] = array(
      'id' => $row['id'],
      'title' => $row['title'],
      'release_date' => $row['release_date'],
      'startdatum' => $row['startdatum'],
      'enddatum' => $row['enddatum']
    );
  }
    return $ausgabe;
  }

  public function getVorstellungen($filmid, $kino){
    $db = db_datenbank::get_instanz();
    $filmid = $db->escape($filmid);

    $sql = "SELECT * FROM vorstellungen WHERE film = '{$filmid}' AND kinosaal = '{$kino}'";

    $ergebnis = $db->query($sql);

    $ausgabe = array();
    while($row = mysqli_fetch_assoc($ergebnis)){
      $ausgabe[] = array(
        'id' => $row['id'],
        'input' => $row['input']

      );
    }
    return $ausgabe;
  }

  public function getSpielplanDetail($filmid){
    $db = db_datenbank::get_instanz();
    $filmid = $db->escape($filmid);

    $sql = "SELECT filme.title, filme.id, laufzeit.film, laufzeit.startdatum, laufzeit.enddatum
        FROM filme
        JOIN laufzeit ON laufzeit.film = filme.id
        WHERE filme.id = '{$filmid}'";

    $sql = $db->query($sql);

    $ausgabe = mysqli_fetch_assoc($sql);
    return $ausgabe;
  }

  public function safeSpielplan(array $daten){
    $db = db_datenbank::get_instanz();
    $movie = $this->getSpielplanDetail($daten['filmid']);

    $thisDate = array();
    for ($i = strtotime($movie['startdatum']); $i <= strtotime($movie['enddatum']); $i = $i + 86400 ) {
      $thisDate[] = date( 'Y-m-d', $i );
    }

    if(!empty($daten['filmid'])){
      $db->query("DELETE FROM vorstellungen WHERE film = '{$daten['filmid']}'");
    }

    $grundpreis = backend_settings::getDefaultShowPrice();
    
    $sql = "INSERT INTO vorstellungen (film, kinosaal, datum, uhrzeit, input, preis) VALUES ";

    $thisTime = array();
    $time = '14:30';
    for ($x = 0; $x < 21; $x++) {
      $newTime = date('H:i:s',strtotime($time . ' +30 minutes'));
      $thisTime[] = $newTime;
      $time = $newTime;
    }

    if(!empty($daten['spielzeit'])){
      $i = 0;
      $count = count($daten['spielzeit']);

      foreach($daten['spielzeit'] as $key => $value){
        $arr = explode("-",$key);
        $sql .= "('".$daten['filmid']."','".$daten['kino']."','".$thisDate[$arr[0]]."','".$thisTime[$arr[1]]."','".$key."','".$grundpreis."')";
        $i++;
        if($count > $i){
          $sql .= ", ";
        }
      }
      if($db->query($sql)){
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}
