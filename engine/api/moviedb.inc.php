<?php
/**
* Schnittstelle zur MovieDB (Dokumentation: https://developers.themoviedb.org/3/)
**/
class api_moviedb {

  private $methode;
  private $lang;
  private $filmID;
  private $requestUrl;
  private $baseUrl = 'https://api.themoviedb.org/3/movie/';
  private $searchUrl = 'https://api.themoviedb.org/3/search/movie?';
  private $adultContent = 'false';
  private $apiKey = MOVIEDB_APIKEY;

  public function movieRequest($methode, $filmID, $lang = 'de'){
    $this->methode = $methode;
    $this->filmID = $filmID;
    $this->lang = $lang;

    if($this->methode=='details'){
      $this->requestUrl = $this->baseUrl.$this->filmID."?api_key=".$this->apiKey."&language=".$this->lang;
    }

    if($this->methode=='cast'){
      $this->requestUrl = $this->baseUrl.$this->filmID."/credits?api_key=".$this->apiKey."&language=".$this->lang;
    }

    if($this->methode=='trailers'){
      $this->requestUrl = $this->baseUrl.$this->filmID."/videos?api_key=".$this->apiKey."&language=".$this->lang;
    }

    if($this->methode=='images'){
      $this->requestUrl = $this->baseUrl.$this->filmID."/images?api_key=".$this->apiKey."&language=".$this->lang;
    }

    if(!empty($this->requestUrl)){
      $data = self::requestMovieDbApi($this->requestUrl);
      return $data;
    }
  }

  // Allgemeine Suchanfrage an MovieDB
  public function searchMovie($title) {
    $this->requestUrl = $this->searchUrl."api_key=".$this->apiKey."&include_adult=".$this->adultContent."&page=1&query=".urlencode($title);
    $ergebnis = $this->requestMovieDbApi($this->requestUrl);
    return $ergebnis;
  }

  // RequestUrl bei movieDB-Api anfrangen und Inhalt returnen
  protected function requestMovieDbApi($url){

    $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_POSTFIELDS => "{}",
        ));

        $result = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        $response = json_decode($result, true);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $response;
        }
  }

}
