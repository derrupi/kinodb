<?php

class url_urlhandler {

  private $page = 'index'; //default page
  private $menue = array("home","tickets","filme","kontakt","moviedetail", "ticketsdetail","booking","userlogin","spielplan","benutzer", "newPasswort","filme","laufzeit","laufzeitIndex",
  "spielplanDetail","moviedetail","neuerfilm", "neuerfilm2","logout","kino","kinoNew","settings","showDetail");
  private $error = '404';

  public function getUrl($id){
      if(url_urlhandler::checkUrl($id)){
        $this->page = $id;
      } else {
        $this->page = $this->error;
      }
    return $this->page;
  }

  public function checkUrl($page){
    if(in_array($page, $this->menue)){
      return true;
    } else {
      return false;
    }
  }

}
