<?php
class film_filme
{

  public $daten;

  public function __construct(array $daten){
    foreach ($daten as $key => $value){
      $this->$key = $value;
    }
  }

  public function getMovieOverview($datum = ''){
    $db = db_datenbank::get_instanz();
    $datum = $db->escape($datum);
    $datum = helper::dataSQL($datum);

    $sql = "SELECT f.*, l.startdatum, l.enddatum
      FROM filme f
      JOIN laufzeit l ON l.film = f.id
      WHERE EXISTS (SELECT * FROM vorstellungen v WHERE v.film = f.id AND v.datum = '$datum')
      ";
    $ergebnis = $db->query($sql);
    while($row = mysqli_fetch_assoc($ergebnis)){
      $ausgabe[] = array(
        'id' => $row['id'],
        'runtime' => $row['runtime'],
        'poster_path' => $row['poster_path'],
        'title' => $row['title'],
        'tagline' => $row['tagline'],
        'discription' => $row['overview'],
        'release_date' => $row['release_date'],
        'show' => self::getShow($row['id'], $datum)
      );
    }
    if(mysqli_num_rows($ergebnis) == 0){
      return false;
    }
    return $ausgabe;
  }

  public function getMovieList($option = '', $datum = ''){
    $db = db_datenbank::get_instanz();

    $sql = "SELECT *
      FROM filme f";

    if(empty($datum)){
      $sql .= " WHERE f.release_date >= CURDATE()";
    } else {
      $sql .= " WHERE f.release_date >= '{$datum}'";
    }

    if(!empty($option)){
      if(empty($datum)){
        $sql .= " AND";
      } else {
        $sql .= " WHERE";
      }
      $sql .= " NOT EXISTS (SELECT * FROM laufzeit l
          WHERE l.film = f.id)";
    }

      $sql .= " ORDER BY f.release_date";

    $arr = $db->query($sql);
    $ergebnis = array();

    while($row = mysqli_fetch_assoc($arr)){
      $id = $row['id'];
      $show['show'] = self::getShow($row['id'], $datum);
      $genres['genres'] = self::getGenres($id);
      $ergebnis[] = array_merge($row, $genres, $show);
    }

    return $ergebnis;
  }

  public function getGenres($id){
    $db = db_datenbank::get_instanz();

    $sql = "SELECT * FROM genres g JOIN filme_genres fg WHERE g.id = fg.genre AND fg.film = '{$id}'";
    $arr = $db->query($sql);
    $ergebnis = array();

      while($row = mysqli_fetch_assoc($arr)){
        $ergebnis[] = $row;
      }

    return $ergebnis;
  }

  public function getShow($filmid, $datum = ''){
    $db = db_datenbank::get_instanz();
    $filmid = $db->escape($filmid);

    $sql = "SELECT v.*, k.saalnr FROM vorstellungen v, kinosaele k
    WHERE v.film = '{$filmid}'
    AND k.id = v.kinosaal";
    if(!empty($datum) && $datum != 'no'){
      $sql .= " AND datum = '{$datum}'";
    } elseif(!empty($datum) && $datum = 'no'){
      $sql .= '';
    } else {
      $datum = date("Y-m-d");
      $sql .= " AND datum = '{$datum}'";
    }
    $arr = $db->query($sql);


    $ergebnis = array();

      while($row = mysqli_fetch_assoc($arr)){
        $ergebnis[] = $row;
        // $ergebnis['seats'] = film_tickets::countReservations('156');
      }
      return $ergebnis;
  }

  public function setShowPrice($daten){
    $db = db_datenbank::get_instanz();

    $count = count($daten['vorstellungsid']);
    for($i=0;$i<$count;$i++){
      $db->query("UPDATE vorstellungen SET preis = '".$daten['preis'][$i]."' WHERE id = '".$daten['vorstellungsid'][$i]."'");
    }
    return true;
  }

  public function getMovie($id){
    $db = db_datenbank::get_instanz();
    $id = $db->escape($id);

    $sql = "SELECT * FROM filme WHERE id = '{$id}'";
    return mysqli_fetch_assoc($db->query($sql));
  }

  public function getMoviesInFuture(){
    $db = db_datenbank::get_instanz();
    $ausgabe = array();

    $sql = "SELECT * FROM filme WHERE release_date > CURDATE() LIMIT 3";
    $ergebnis = $db->query($sql);
    while($row = mysqli_fetch_assoc($ergebnis)){
      $ausgabe[] = array(
        'id' => $row['id'],
        'poster' => $row['poster_path'],
        'title' => $row['title'],
        'tagline' => $row['tagline'],
        'discription' => $row['overview'],
        'release_date' => $row['release_date']
      );
    }
    return $ausgabe;
  }

  public function addMovie(){
    $db = db_datenbank::get_instanz();
    $daten = new daten_validieren();

    $ausgabe = array();

    $ausgabe['moviedb_id'] = $daten->input("MovieDB-Id", $this->poster_path, 1);
    $ausgabe['poster_path'] = $daten->input("Poster", $this->poster_path, 1);
    $ausgabe['title'] = $daten->input("Titel", $this->title, 1);
    $ausgabe['tagline'] = $daten->input("Tagline", $this->title, 0);
    $ausgabe['original_language'] = $daten->input("Originalsprache", $this->original_language, 0);
    $ausgabe['original_title'] = $daten->input("Originaltitel", $this->original_title, 0);
    $ausgabe['release_date'] = $daten->inputDate("Veröffentlichung", $this->release_date, 1);
    $ausgabe['runtime'] = $daten->input("Laufzeit", $this->runtime, 1);
    $ausgabe['budget'] = $daten->input("Budget", $this->budget, 0);
    $ausgabe['homepage'] = $daten->input("Website", $this->homepage, 0);
    $ausgabe['overview'] = $daten->input("Handlung", $this->overview, 1);
    $ausgabe['trailer'] = $daten->input("Trailer", $this->trailer, 0);

    if(!helper::multiKeyExists($ausgabe, "status")){

      $max = count($ausgabe);
      $i = 1;

      $sql = "INSERT INTO filme SET ";
      foreach($ausgabe as $key => $item){
        $sql .= $key." = '".$item."'";
        if ($i++ != $max) {
          $sql .= ", ";
        } else {
          $sql .= " ";
        }
      }
      $db->query($sql);
      return true;
    } else {
      return $ausgabe;
    }
  }
}
