<?php

class film_laufzeit {

  public function getAllRuntimes() {
    $db = db_datenbank::get_instanz();

    $sql = "SELECT * FROM filme
      WHERE NOT EXISTS (SELECT id FROM laufzeit WHERE film = filme.id)";

    $ergebnis = $db->query($sql);

      $ausgabe = array();

        while($row = mysqli_fetch_assoc($ergebnis)){

          $ausgabe = array(
            'id' => $row['id'],
            'title' => $row['title'],
            'release_date' => $row['release_date']
          );
        }

    return $ausgabe;
  }

  public function setRuntime(array $datensatz){
    $db = db_datenbank::get_instanz();

    $datensatz = $db->escape($datensatz);

    $eintrag["film"] = $datensatz['filmid'];
    $eintrag["startdatum"] = helper::dataSQL($datensatz['fromdate']);
    $eintrag["enddatum"] = helper::dataSQL($datensatz['todate']);

      if($db->insert("laufzeit",$eintrag)){
        return true;
      } else {
        return false;
      }
  }

}
