<?php

 class film_tickets {

  public function getActiveTickets($user = ''){
    $db = db_datenbank::get_instanz();
    $user = $db->escape($user);

    $sql = "SELECT res.*, COUNT(res.reservierungsnummer) AS tickets,
          ben.email, ben.vorname, ben.nachname,
          vor.uhrzeit, vor.datum,
          f.title
      FROM reservierungen res, benutzer ben, vorstellungen vor, filme f
      WHERE ben.id = res.kunde
      AND vor.id = res.vorstellung
      AND vor.datum >= CURDATE()
      AND f.id = vor.film";

      if(!empty($user)){
        $sql .= " AND ben.email = '".$user."'";
      }

      $sql .= " GROUP BY res.reservierungsnummer
      ORDER BY vor.datum, vor.uhrzeit";

    $ausgabe = array();
    $ergebnis = $db->query($sql);
    while($row = mysqli_fetch_assoc($ergebnis)){
      $ausgabe[] = array(
        'email' => $row['email'],
        'vorname' => $row['vorname'],
        'nachname' => $row['nachname'],
        'reservierungscode' => $row['reservierungsnummer'],
        'tickets' => $row['tickets'],
        'vorstellungDatum' => $row['datum'],
        'vorstellungUhrzeit' => $row['uhrzeit'],
        'title' => $row['title'],
        'status' => $row['abgeholt']
      );
    }
    return $ausgabe;
  }

  public function deleteReservation($code){
    $db = db_datenbank::get_instanz();
    $code = $db->escape($code);

    $sql = "DELETE FROM reservierungen WHERE reservierungsnummer = '".$code."'";

    if($db->affectedQuery($sql) >= 1){
      return true;
    } else {
      return false;
    }
  }

  public function getReservation($code){
    $db = db_datenbank::get_instanz();
    $code = $db->escape($code);

    $sql = "SELECT res.*, ben.vorname, ben.nachname, ben.email, vor.uhrzeit, vor.datum
      FROM reservierungen res, benutzer ben, vorstellungen vor
      WHERE ben.id = res.kunde
      AND res.reservierungsnummer = '".$code."'";

      $row = mysqli_fetch_assoc(($db->query($sql)));

      return array(
        'email' => $row['email'],
        'vorname' => $row['vorname'],
        'nachname' => $row['nachname'],
        'reservierungscode' => $row['reservierungsnummer'],
        'vorstellungDatum' => $row['datum'],
        'vorstellungUhrzeit' => $row['uhrzeit']
      );
    }

    public function setTicketsPicked($code){
      $db = db_datenbank::get_instanz();
      $code = $db->escape($code);

      $sql = "UPDATE reservierungen SET abgeholt = '1' WHERE reservierungsnummer = '".$code."'";
      if($db->query($sql)){
        return true;
      } else {
        return false;
      }

    }

    public function printTicket($code){
      $db = db_datenbank::get_instanz();
      $code = $db->escape($code);

      $sql = "SELECT * FROM reservierungen res, filme f, vorstellungen vor
        WHERE reservierungsnummer = '".$code."' AND vor.id = res.vorstellung AND f.id = vor.film";
      $ergebnis = $db->query($sql);
      $ergebnis2 = $db->query($sql);

      $mpdf = new \Mpdf\Mpdf();
      $er = mysqli_fetch_assoc($ergebnis);
      $html .= "<h2><font size='18px'>Reservierungsbestätigung</font></h2>";
      $html .= "<br />";
      $html .= "<b>{$er['title']}</b><br />";
      $html .= "<br /><br />";
      $html .= "<b>Reservierungsnummer:</b> ".$er['reservierungsnummer']."<br />";
      $html .= "<b>Vorstellungsdatum:</b> ".$er['datum'];
      $html .= "<br /><b>Uhrzeit:</b> ".$er['uhrzeit'];
      $html .= "<br /><b>Kino:</b> ".$er['kinosaal'];
      $html .= "<hr />";
      $html .= "<b>Reservierte Sitze</b><br />";
      while($row = mysqli_fetch_assoc($ergebnis2)){
        $html .= "Reihe: ".$row['reihe']." Sitzplatz: ".$row['sitzplatz']."<br />";
      }
      $html .= "<br /><br />";
      $html .= "Vielen Dank für Deinen Besuch in unserem Kino und gute Unterhaltung!";
      $mpdf->WriteHTML($html);
      return $mpdf->Output();

    }

    static public function countReservations($vorstellung){
      $db = db_datenbank::get_instanz();
      $vorstellung = $db->escape($vorstellung);

      $sql = "SELECT COUNT(vorstellung) as tickets FROM reservierungen WHERE vorstellung = '".$vorstellung."'";
      $ergebnis = $db->query($sql);
      $row = mysqli_fetch_assoc($ergebnis);
        return $row['tickets'];
    }
 }
