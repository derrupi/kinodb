<?php
include "functions.inc.php";

$curlurl = "https://api.themoviedb.org/3/genre/movie/list?";

$genres = movieRequest($curlurl);

foreach($genres['genres'] as $key => $value){
  $datensatz = array(
    'moviedb_id' => $value['id'],
    'name' => $value['name']
  );
  insert("genres", $datensatz);
  unset($datensatz);
}
