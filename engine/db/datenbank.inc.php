<?php

class db_datenbank
{
  // enthält die Datenbank verbinden.
  private $db;

  // Singleton Implementieren
  // Vermeidet mehrfache Erstellung des selben Objekts
  // Hier nötig, um nicht mehrere Datenbankverbindungen gleichzeitig
  // zu öffnen
  private static $instanz;

  public static function get_instanz(){
    if(!self::$instanz){
      self::$instanz = new db_datenbank();
    }
    return self::$instanz;
  }


  private function __construct() {
    $this->verbinden();
  }

  public function verbinden(){
    //Verbindung zu MySQL-Datenbank aufbauen
    $this->db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
    //Datenbank mitteilen, welchen Zeichensatz wir verwende
    $this->db->set_charset("utf8");
  }

  public function query($sql_befehl){

    $ergebnis = $this->db->query($sql_befehl) or die($this->db->error);
    return $ergebnis;
  }

  public function affectedQuery($sql_befehl){
    $ergebnis = $this->db->query($sql_befehl) or die($this->db->error);
    return $this->db->affected_rows;
  }

  // Escape Funktion, die ein ganzes Array escapped (überlicherweise $_POST)
  // Das Wort "array" vor dem ersten Parameter ist ein Type-Hind
  // Dadurch wird man beim aufrufen der Funktion gezwungen, ein array als ersten Parameter zu übergeben
  public function escape($werte) {
    if(is_array($werte)){
      foreach ($werte as $index => $wert) {
        $werte[$index] = $this->db->real_escape_string($wert);
      }
    } else {
      $werte = $this->db->real_escape_string($werte);
    }
    return $werte;
  }

  // Insert into SQL
  public function insert($tableName, $datensatz) {
    $sql = "INSERT INTO {$tableName} SET ";
    $i = 0;
      foreach($datensatz as $spalte => $wert) {
        if($i != 0) $sql .= ",";
        if (is_null($wert)) {
          $sql .= "{$spalte} = NULL";
        } else {
          $sql .= "{$spalte} = '{$wert}'";
        }
        $i++;
      }
    if($this->db->query($sql)){
      return true;
    } else {
      return false;
    }
  }

}
