<?php

class kinosaal_kino
{

  static public function getAllKinos(){
    $db = db_datenbank::get_instanz();

    $sql = "SELECT * FROM kinosaele";
    $ergebnis = $db->query($sql);

      while($row = mysqli_fetch_assoc($ergebnis)){
        $ausgabe[] = array(
          'id' => $row['id'],
          'saalnr' => $row['saalnr'],
          'sitzplaetze' => $row['sitzplaetze'],
          'reihen' => $row['reihen']
        );
      }
    return $ausgabe;
  }

  public function getSeats($showId){
    $db = db_datenbank::get_instanz();

    $show = $db->escape($showId);
    $sql = "SELECT kino.*, vor.kinosaal FROM vorstellungen vor
      JOIN kinosaele kino
      WHERE vor.id = '{$show}'
      AND kino.id = vor.kinosaal";

      $ergebnis = mysqli_fetch_assoc($db->query($sql));
        $arr = array();
        $arr['sitzplaetze'] = $ergebnis['sitzplaetze'];
        $arr['reihen'] = $ergebnis['reihen'];
    return $arr;
  }

  public function getReservatedSeats($showId){
    $db = db_datenbank::get_instanz();

    $show = $db->escape($showId);

    $res = array();
    $sql = "SELECT sitzplatz, reihe FROM reservierungen WHERE vorstellung = '{$show}'";
    $ergebnis = $db->query($sql);
      while($row = mysqli_fetch_assoc($ergebnis)){
        $res[] = $row['reihe']."-".$row['sitzplatz'];
      }
    return $res;

  }

  public function getLastKino(){
    $db = db_datenbank::get_instanz();

    $sql = "SELECT MAX(saalnr) AS saal FROM kinosaele";
    $ergebnis = mysqli_fetch_assoc($db->query($sql));

    return $ergebnis['saal'] + 1;
  }

  public function addKino(array $daten){
    $db = db_datenbank::get_instanz();
    $daten = $db->escape($daten);

    $sql = "INSERT INTO kinosaele (saalnr, sitzplaetze, reihen) VALUES('{$daten['saalnr']}','{$daten['sitzplaetze']}','{$daten['reihen']}')";

    if($db->query($sql)){
      return true;
    } else {
      return false;
    }

  }
}
