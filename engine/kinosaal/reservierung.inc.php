<?php
 class kinosaal_reservierung {

   public $test = null;
   private $buchstaben = 'abcdefghijklmnopqrstuvwxyz';

   public function addReservation($resSeats, $showId, $kunde){
     $db = db_datenbank::get_instanz();
     $bookingNumber = helper::getRandomCode();

     $ergebnis = array();
     $resSeats = $db->escape($resSeats);
     $showId = $db->escape($showId);
     $kunde = $db->escape($kunde);
     $reiheTransform = '';
     $resSeats = explode(",", $resSeats);
       foreach ($resSeats as $seat => $value) {
         $teile = explode("-", $value);
         $reihe = strtolower($teile[0]);
            $reiheTransform = array_search($reihe, range('a','z'))+1;
            if(self::checkIfReservationExists($reiheTransform, $teile[1], $showId)){
              $db->query("INSERT INTO reservierungen (kunde, vorstellung, reihe, sitzplatz, reservierungsnummer) VALUES('{$kunde}','{$showId}','{$reiheTransform}','{$teile[1]}','{$bookingNumber}')");
              $ergebnis[] = 'War erfolgreich';
            } else {
              $ergebnis[] = 'War nicht erfolgreich';
            }
       }
       return $ergebnis;
     }

    public function checkIfReservationExists($row, $seat, $showId){
      $db = db_datenbank::get_instanz();

      $row = $db->escape($row);
      $seat = $db->escape($seat);
      $showId = $db->escape($showId);

      $sql = "SELECT * FROM reservierungen
          WHERE vorstellung = '{$showId}'
          AND reihe = '{$row}'
          AND sitzplatz = '{$seat}'";

        $ergebnis = $db->query($sql);
          if(mysqli_num_rows($ergebnis) >= 1){
            return false;
          } else {
            return true;
          }
    }

 }
