<?php

class helper {

  // Durchsucht Mehrdiminsionale Arrays (Wert)
  public static function in_array_r($needle, $haystack, $strict = true) {
  	    foreach ($haystack as $item) {
  	        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && self::in_array_r($needle, $item, $strict))) {
  	            return true;
  	        }
  	    }
  	    return false;
  }

  // Durchsucht Mehrdiminsionale Arrays (Key)
  public static function multiKeyExists(array $arr, $key) {

    if (array_key_exists($key, $arr)) {
        return true;
    }

    foreach ($arr as $element) {
        if (is_array($element)) {
            if (self::multiKeyExists($element, $key)) {
                return true;
            }
        }

    }

    return false;
  }

  public static function getBarcode($number){
    $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
    $barcode = '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($number, $generator::TYPE_CODE_128)) . '">';
    return $barcde;
  }

  public static function sec_session_start() {
      $session_name = 'sec_session_id';   // vergib einen Sessionnamen
      $secure = SECURE;
      // Damit wird verhindert, dass JavaScript auf die session id zugreifen kann.
      $httponly = true;
      // Zwingt die Sessions nur Cookies zu benutzen.
      if (ini_set('session.use_only_cookies', 1) === FALSE) {
          exit();
      }
      // Holt Cookie-Parameter.
      $cookieParams = session_get_cookie_params();
      session_set_cookie_params($cookieParams["lifetime"],
          $cookieParams["path"],
          $cookieParams["domain"],
          $secure,
          $httponly);
      // Setzt den Session-Name zu oben angegebenem.
      session_name($session_name);
      session_start();            // Startet die PHP-Sitzung
      session_regenerate_id();    // Erneuert die Session, löscht die alte.
  }

  public static function getRandomCode($laenge = 10){

    $tempCode = 'abcdefghjkmnpqrstuvwxyz';
    $tempCode .= strtoupper($tempCode);
    $tempCode .= '1234567890';

    $tempCode = str_shuffle($tempCode);
    $tempCode = substr($tempCode, 0, $laenge);
    return $tempCode;
    }

  public static function checkNewPasswortLink($code){
    $db = db_datenbank::get_instanz();
    $code = $db->escape($code);

    $now = time();

    $time = $now - (24 * 60 * 60);

    $sql = "SELECT * FROM benutzer_passwort WHERE code = '{$code}' AND active = '1' AND timestamp >= '{$time}'";
    $ergebnis = $db->query($sql);

    $count = mysqli_num_rows($ergebnis);
    if($count < 1){
      return false;
    } else {
      return true;
    }
  }

  public static function checkLogin(){
    if(isset($_SESSION['user_id'],$_SESSION['benutzer'])){
      //Benutzer ist nicht eingeloggt. -> Umleiten zum Login
      return true;
      exit;
    } else {
      return false;
    }
  }

  public static function sortArray($array = array (), $art, $reinfolge=SORT_DESC) {
      if(is_array($array)==true){
          foreach ($array as $key => $value) {
              if(is_array($value)==true){
                  foreach ($value as $kk => $vv) {
                      ${$kk}[$key]  = strtolower( $value[$kk]);
                  }
              }
          }
      }
      array_multisort(${$art}, $reinfolge, $array);
      return $array;
  }

  public static function dataSQL($input){
    $newDate = date('Y-m-d', strtotime($input));
    return $newDate;
  }
}
