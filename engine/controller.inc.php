<?php
class controller
{

  private $params = array();
  private $request = null;
  private $template = '';
  private $menue_list = array();
  private $movie;
  private $show;
  private $booking;
  private $userdaten;
  private $userform;
  private $view;
  private $action;
  private $code;
  private $data;

  public function __construct($request){
        $this->request = $request;
        $page = new url_urlhandler();
        $this->template = (!empty($request['p'])) ? $page->getUrl($request['p']) : 'home';
        $this->view = (!empty($request['view'])) ? $request['view'] : 'frontend';
        $this->movie = (!empty($request['id'])) ? $request['id'] : '';
        $this->show = (!empty($request['show'])) ? $request['show'] : '';
        $this->booking = (!empty($request['resSeats'])) ? $request['resSeats'] : '';
        $this->action = (!empty($request['action'])) ? $request['action'] : '';
        $this->code = (!empty($request['code'])) ? $request['code'] : '';
        $this->userdaten = (!empty($request['userform']) xor !empty($request['userid']) xor !empty($request['code'])) ? array(
          'code' => $this->code,
          'userid' => (!empty($request['userid'])) ? $request['userid'] : '',
          'userform' => (!empty($request['userform'])) ? $request['userform'] : '',
          'username' => (!empty($request['username'])) ? $request['username'] : '',
          'vorname' => (!empty($request['vorname'])) ? $request['vorname'] : '',
          'nachname' => (!empty($request['nachname'])) ? $request['nachname'] : '',
          'email' => (!empty($request['email'])) ? $request['email'] : '',
          'passwort' => (!empty($request['passwort'])) ? $request['passwort'] : '',
          'passwortValidate' => (!empty($request['passwortValidate'])) ? $request['passwortValidate'] : ''
        ) : '';

        $this->data = $request;
    }

  public function __get($varName){

     if (!array_key_exists($varName,$this->data)){
         //this attribute is not defined!
         throw new Exception('this attribute is not defined!');
     }
     else return $this->data[$varName];

  }

  public function __set($varName,$value){
     $this->data[$varName] = $value;
  }

  public function display(){


    if($this->view == "frontend"){
        $core = new Dwoo\Core();
        switch($this->template){
            case 'filme':
              $film = new film_filme($this->data);
              $datum = (!empty($this->data['datum'])) ? $this->data['datum'] : date("d.m.Y",time());

              $filme = $film->getMovieOverview($datum);
              $this->template = 'filme.tpl';

              if(is_array($filme)){

                $this->params['movies'] = $filme;

              } else {
                $this->params['status'] = 0;
              }
              $this->params['sDate'] = (array_key_exists("datum", $this->data)) ? strtotime($this->data['datum']) : '';
              $this->params['date'] = strtotime(date("Y-m-d"));
              // print_r($this->params['parameters']);
              break;

            case 'home':
              $this->template = 'home.tpl';
              $film = new film_filme($this->data);
              $this->params['futureMovies'] = $film->getMoviesInFuture();
              $filme = $film->getMovieOverview(date("d.m.Y",time()));
              $this->params['movies'] = $filme;

              break;

            case 'userlogin':
              $this->template = 'userlogin.tpl';

              if(!empty($this->userdaten)){

                $this->params['error'] = '';
                  if($this->userdaten['userform'] == 1){
                    $user = new user_user($this->userdaten);
                    $this->params['error'] = $user->addUser();
                  }
                  if($this->userdaten['userform'] == 2){
                    $userLogin = new user_user($this->userdaten);
                    $this->params['error'] = $userLogin->login();

                  }
                  if($this->userdaten['userform'] == 3){
                    $userPasswort = new user_user($this->userdaten);
                    $this->params['error'] = $userPasswort->sendNewPassword();
                  }

              }
              break;

            case 'newPasswort':
              $this->template = 'newPasswort.tpl';
                $this->params['code'] = $this->code;
                if(!helper::checkNewPasswortLink($this->code)){
                  $this->params['error'] = array(
                    'error' => array(
                      'status' => 0,
                      'error' => 'Dein Link ist leider ungültig oder abgelaufen'
                    )
                  );

                } else {
                  $user = new user_user($this->userdaten);
                  if(!empty($this->userdaten['passwort'])){
                    $this->params['error'] = $user->setNewPassword();
                  }
                }
              break;

            case 'booking':
              $booking = new kinosaal_reservierung();
              $resSeats = $booking->addReservation($this->booking,$this->show, $this->data['kunde']);
              $bookingNumber = helper::getRandomCode();
              $this->template = 'booking.tpl';
              break;

            case 'ticketsdetail':
              $this->template = 'ticketsdetail.tpl';
              $theater = new kinosaal_kino();
              $theaterConfig = $theater->getSeats($this->show);
              $this->params['theater'] = $theaterConfig;
              $resSeats = $theater->getReservatedSeats($this->show);
              $film = new film_filme($this->data);
              $filme = $film->getMovie($this->movie);
              $this->params['movie'] = $filme;
              $this->params['show'] = $this->show;
              $this->params['resSeats'] = $resSeats;

              $tickets = new film_tickets();

              if(user_user::checkLogin()){
                if(user_user::checkUserExist($_SESSION['benutzer'])){
                  $userdaten = new user_userdaten();

                  $this->params['user'] = $userdaten->getUser($_SESSION['benutzer']);
                  $this->params['user']['status'] = "ja";
                } else {
                  $this->params['user']['status'] = "nein";
                }

              } else {
                $this->params['user']['status'] = "nein";
              }

              break;

            case 'moviedetail':
              $this->template = 'moviedetail.tpl';
              $movie = new film_filme($this->data);
              $this->params['movie'] = $movie->getMovie($this->data['filmid']);
              $this->params['show'] = helper::sortArray($movie->getShow($this->data['filmid'], "no"),"datum",SORT_ASC);
              $this->params['genres'] = $movie->getGenres($this->data['filmid']);
              break;

            case 'tickets':
              $this->template = 'tickets.tpl';
              if(user_user::checkLogin()){
                if(user_user::checkUserExist($_SESSION['benutzer'])){
                  $tickets = new film_tickets();
                  if(!empty($this->data['action']) && $this->data['action']=="print"){

                    $tickets->printTicket($this->data['reservierungscode']);

                  }
                  if(!empty($this->data['action']) && $this->data['action']=="delete"){
                    if($tickets->deleteReservation($this->data['reservierungscode'])){
                      $this->params['error'] = "Reservierung wurde gelöscht!";
                      $user = user_user::getUser($_SESSION['benutzer']);
                      $email = new user_sendmail(array(
                        'vorname' => $user['vorname'],
                        'nachname' => $user['nachname'],
                        'code' => $this->data['reservierungscode'],
                        'email' => $user['email'],
                        'template' => 'ticketStorno',
                        'subject' => 'Deine Tickets wurden storniert!'
                      ));
                        $email->sendMail();
                    } else {
                      $this->params['error'] = "Reservierung konnte nicht gelöscht werden!";
                    }
                  }


                  $this->params['daten'] = $tickets->getActiveTickets($_SESSION['benutzer']);
                }
              } else {
                header("Location: index.php?p=userlogin");
              }

              break;

            default:


        }
        /* Load the menue */
        $this->menue_list[] = array('id' => 1, 'visible' => 1, 'name' => 'Home', 'url' => 'home', 'content' => 'home.tpl');
        $this->menue_list[] = array('id' => 2, 'visible' => 1, 'name' => 'Filme', 'url' => 'filme', 'content' => 'filme.tpl');
        $this->menue_list[] = array('id' => 3, 'visible' => 1, 'name' => 'Tickets', 'url' => 'tickets', 'content' => 'tickets.tpl');
        $this->menue_list[] = array('id' => 4, 'visible' => 1, 'name' => 'Login', 'url' => 'userlogin', 'content' => 'userlogin.tpl');
        $this->menue_list[] = array('id' => 5, 'visible' => 0, 'name' => 'Filmdetail', 'url' => 'moviedetail', 'content' => 'moviedetail.tpl');
        $this->menue_list[] = array('id' => 6, 'visible' => 0, 'name' => 'Tickets', 'url' => 'ticketsdetail', 'content' => 'ticketsdetail.tpl');
        $this->menue_list[] = array('id' => 7, 'visible' => 0, 'name' => 'Userlogin', 'url' => 'userlogin', 'content' => 'userlogin.tpl');
        $this->params['menue_type_list'] = $this->menue_list;


        return $core->get('theme/frontend/'.$this->template, $this->params);
    }

    if($this->view == "backend"){
      $backend = new Dwoo\Core();

      switch($this->template){

          case 'home':
            $this->template = 'home.tpl';
          break;

          case 'spielplan':
            $this->template = 'spielplan.tpl';
            $movies = new backend_spielplan();
            $this->params['movies'] = $movies->getSpielplan();
          break;

          case 'benutzer':
            $this->template = 'kundenUebersicht.tpl';
            $customers = new backend_kunden();

            if(!empty($this->userdaten["userid"])){
              $user = new user_user($this->userdaten);
              if($this->action=="delete"){
                $this->params['error'] = $user->deleteUser();
              }
              if($this->action=="unlock"){
                $this->params['error'] = $user->unlockUser();
              }
              if($this->action=="newPasswort"){
                $this->params['error'] = $user->sendNewPassword();
              }
            }
            $this->params['users'] = $customers->getCustomers();

          break;

          case 'filme':
            $this->template = 'filmeIndex.tpl';

            $movies = new film_filme($this->data);
            $this->params['movies'] = $movies->getMovieList();
          break;

          case 'neuerfilm':
            $this->template = "filmNeu.tpl";
            if(!empty($this->data['action']) && $this->data['action']=="step2"){
              if($this->data['methode'] == 1){
                $movieRequest = new api_moviedb();
                $film = $movieRequest->searchMovie($this->data['filmtitel']);
                $this->params['count'] = count($film['results']);
                $this->params['filme'] = $film['results'];
              }
            }
          break;

          case 'neuerfilm2':
            $this->template = "filmNeu2.tpl";
            if(!empty($this->data['filmid'])){
              $movie = new api_moviedb();
              $this->params['film'] = $movie->movieRequest('details',$this->data['filmid']);
              $this->params['cast'] = $movie->movieRequest('cast',$this->data['filmid']);

              $arr = $movie->movieRequest('trailers',$this->data['filmid']);
              if(count($arr['results']) >= 1){
                $this->params['trailer'] = $arr['results'][0]['key'];
              } else {
                $this->params['trailer'] = "Kein Trailer auf YouTube gefunden";
              }
              $this->params['images'] = $movie->movieRequest('images',$this->data['filmid']);
              $this->params['OverviewCount'] = strlen($this->params['film']['overview']);
            }

            if(!empty($this->data['action']) && $this->data['action']=="speichern"){
              $movies = new film_filme($this->data);

              $ergebnis = $movies->addMovie();
              if($ergebnis === true){
                $this->params['error'] = 1;
              } else {
                $this->params['film'] = $this->data;
                $this->params['trailer'] = $this->data['trailer'];
                $arr = array();
                foreach ($ergebnis as $key => $value){
                  if(is_array($value)){
                    foreach ($value as $k => $v){
                      if($k == "error"){
                        $arr[] = array('error' => $v);
                      }
                    }
                  }
                }
                $this->params['OverviewCount'] = strlen($this->data['overview']);
                $this->params['error'] = $arr;
              }

            }
          break;

          case 'tickets':
            $this->template = 'ticketsIndex.tpl';
            $tickets = new film_tickets();

            if(!empty($this->data['action']) && $this->data['action']=="delete"){
              if($tickets->deleteReservation($this->data['reservierungscode'])){
                $this->params['error'] = "Reservierung wurde gelöscht!";
              } else {
                $this->params['error'] = "Reservierung konnte nicht gelöscht werden!";
              }
            }
            if(!empty($this->data['action']) && $this->data['action']=="edit"){

              print_r($tickets->getReservation($this->data['reservierungscode']));
            }

            if(!empty($this->data['action']) && $this->data['action']=="status"){
              if($tickets->setTicketsPicked($this->data['reservierungscode'])){
                $this->params['error'] = "Reservierung wurde abgeholt!";
              } else {
                $this->params['error'] = "Reservierung konnte nicht geändert werden!";
              }
            }
            if(!empty($this->data['action']) && $this->data['action']=="print"){
              $tickets->printTicket($this->data['reservierungscode']);
            }
            $this->params['tickets'] = $tickets->getActiveTickets();
          break;

          case 'laufzeit':
            $this->template = 'filmeRuntime.tpl';
            $this->params['data'] = $this->data;
            $movie = new film_filme($this->data);
            $this->params['movies'] = $movie->getMovie($this->data['filmid']);

              if(!empty($this->data['action']) && $this->data['action']=="set"){
                $laufzeit = new film_laufzeit();
                $this->params['laufzeit'] = $laufzeit->setRuntime($this->data);

              }
          break;

          case 'laufzeitIndex':
            $this->template = 'filmeRuntimeIndex.tpl';

            $movies = new film_filme($this->data);
            $this->params['movies'] = $movies->getMovieList($option = 1);
          break;

          case 'spielplanDetail':
            $this->template = 'spielplanSet.tpl';


            $this->params['kino'] = kinosaal_kino::getAllKinos();

            $kino = (array_key_exists("kino", $this->data)) ? $this->data['kino'] : '9';
            $this->params['selected_kino'] = $kino;

            $this->params['filmid'] = $this->data['filmid'];
            $v = array();

            $plan = new backend_spielplan();
            if(!empty($this->data['action']) && $this->data['action']=="speichern"){
              $speichern = $plan->safeSpielplan($this->data);
            }
            $vorstellungen = $plan->getVorstellungen($this->data['filmid'], $kino);
            if(!is_null($vorstellungen)){
              foreach($vorstellungen as $key => $value){
                $v[] = $value['input'];
              }
            }
            $this->params['spielplan'] = $plan->getSpielplanDetail($this->data['filmid']);
            $this->params['startdatum'] = strtotime($this->params['spielplan']['startdatum']);
            $this->params['enddatum'] = strtotime($this->params['spielplan']['enddatum']);
            $this->params['vorstellungen'] = $v;
            // echo "<pre>";
            // print_r(  $this->params['vorstellungen']);
            // echo "</pre>";
            $startDate = new DateTime($this->params['spielplan']['startdatum']);
            $endDate = new DateTime($this->params['spielplan']['enddatum']);
            $interval = $startDate ->diff($endDate);
            $this->params['gesamtTage'] = $interval -> format ('%R%a');



            break;

            case 'showDetail':
              $this->template = 'showDetail.tpl';

              $this->params['filmid'] = $this->data['filmid'];

              $film = new film_filme($this->data);

              if(!empty($this->data['action']) && $this->data['action']=="speichern"){
                $film->setShowPrice($this->data);
              }

              $this->params['movie'] = $film->getMovie($this->data['filmid']);
              $this->params['shows'] = $film->getShow($this->data['filmid'], "no");



            break;
            case 'logout':

              $this->template = 'logout.tpl';
              session_unset();
              session_destroy();

            break;

            case 'kino';
              $this->template = 'kino.tpl';

              $kinos = new kinosaal_kino();
              $this->params['kinos'] = $kinos->getAllKinos();

            break;

            case 'kinoNew';
              $this->template = 'kinoNew.tpl';
              $kino = new kinosaal_kino();
              $this->params['lastCinema'] = $kino->getLastKino();

              if(!empty($this->data['action']) && $this->data['action']=="speichern"){
                if($kino->addKino(array(
                  'saalnr' => $this->data['kinosaal'],
                  'sitzplaetze' => $this->data['sitze'],
                  'reihen' => $this->data['reihen']
                ))){
                  $this->params['status'] = 1;
                } else {
                  $this->params['status'] = 0;
                }
              }
            break;

            case 'settings':
              $this->template = "settings.tpl";

              $settings = new backend_settings();

              if(!empty($this->data['action']) && $this->data['action']=="speichern"){
                if($settings->safeSettings(array(
                  'grundpreis' => $this->data['grundpreis']
                ))){

                }
              }
              $this->params['daten'] = $settings->getSettings();

            break;
            case 'login':




            break;
        default;
      }

      if(user_user::checkLogin()){
        if(user_user::checkAdmin($_SESSION['benutzer'])){
          return $backend->get('theme/backend/'.$this->template, $this->params);
        } else {
          $this->template = 'login.tpl';
        }
      } else {
        if(!empty($this->data['action']) && $this->data['action']=="login"){
          $userLogin = new user_user($this->data);
          $this->params['error'] = $userLogin->login();
          $this->template = 'login.tpl';

        }
        $this->template = 'login.tpl';

      }
      return $backend->get('theme/backend/'.$this->template, $this->params);

  }
}




}
