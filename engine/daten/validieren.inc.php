<?php

class daten_validieren {

  public function getUrl($data) {
    $data = filter_var($data, FILTER_VALIDATE_URL);
    if(!$data){
      $ausgabe = $this->error("Die angegebene Url ist nicht gültig");
    } else {
      $ausgabe = self::inputTest($data);
    }
    return $ausgabe;
  }

  public function input($name, $input, $pflicht){
    if(!empty($input)){
      $ausgabe = ucfirst(self::inputTest($input));
    } else {
      if($pflicht == 1){
      $ausgabe = $this->error("Das Feld ".ucfirst($name)." darf nicht leer sein.");
    } else {
      $ausgabe = '';
      }
    }
    return $ausgabe;
  }

  public function inputEmail($name, $input, $pflicht){
      if(!empty($input)){
        if(!filter_var($input, FILTER_VALIDATE_EMAIL)){
          $ausgabe = $this->error("Die eingegeben E-Mailadresse ist ungültig");
        } else {
          $ausgabe = self::inputTest($input);
        }
      } else {
        if($pflicht == 1){
        $ausgabe = $this->error("Das Feld ".ucfirst($name)." darf nicht leer sein.");
        }
      }
    return $ausgabe;
  }

  public function inputDate($name, $input, $pflicht){
    if (!empty($input)){
        if (!preg_match('/[\d]{2}+[\.]+[\d]{2}+[\.]+[\d]{4}/', $input))  {
            $ausgabe = $this->error("Bitte geben Sie das Veröffentlichungsdatum in einen validen Format ein!");
        } else {
            $datum_jetzt = time();
            $datum_dann = explode(".",$input);
            $timestamp_dann = mktime(date("H"),date("i"),date("s"),$datum_dann[1],$datum_dann[0],$datum_dann[2]);
                if($datum_jetzt >= $timestamp_dann){
                    $ausgabe = $this->error('Datum liegt in der Vergangenheit! Film kann nur in der Zukunft gestartet werden!');
                } else {
                   $ausgabe = helper::dataSQL($input);
                }
        }
    } else {
        $ausgabe = $this->error('Bitte geben Sie an, wann der Film veröffentlicht wird!');
    }

    return $ausgabe;
  }

  public function inputPassword($passwort, $passwortValidate){
    $ausgabe = array();
    if(!empty($passwort)){
      if(!empty($passwortValidate)){
        if($passwort == $passwortValidate){
          if (strlen($passwort) < 8) {
            $ausgabe = $this->error("Dein Passwort ist zu kurz!");
          }

          if (!preg_match("#[0-9]+#", $passwort)) {
            $ausgabe = $this->error("Dein Passwort muss mindestens eine Ziffer enthalten.");
          }

          if (!preg_match("#[a-zA-Z]+#", $passwort)) {
            $ausgabe = $this->error("Dein Passwort muss mindestens ein Zeichen enthalten");
          }

          if (!preg_match("#[a-zA-Z]+#", $passwort)) {
            $ausgabe = $this->error("Dein Passwort muss mindestens ein Zeichen enthalten");
          }
          if(!helper::multiKeyExists($ausgabe, "status")){
            $ausgabe = password_hash($passwort, PASSWORD_DEFAULT);
          }
        } else {
          $ausgabe = $this->error("Die eingegebene Passwörter stimmen nicht überein");
        }
      } else {
        $ausgabe = $this->error("Bitte gib dein Passwort nochmals ein!");
      }
    } else {
      $ausgabe = $this->error("Das Passwort darf nicht leer sein");
    }
    return $ausgabe;
  }

  public function inputTest($data) {
    $data = addslashes($data);
    $data = htmlspecialchars($data);
    $data = trim($data);
    return $data;
  }



  private function error($fehlermeldung){
    return array(
      "status" => 0,
      "error" => $fehlermeldung
    );
  }

}
