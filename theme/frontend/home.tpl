{extends "index.tpl"}

{block "content"}
	{include(file='teaser.tpl')}
<div id='programm'>
	<div class='wrapper headline'>
		<div class='row'>
			<h2>Unser Kinoprogramm für {date_format $.now "%d.%m.%Y"}</h2>
		</div>
	</div>
	{include(file='filmeOverview.tpl')}
</div>

<div id='topfilme'>
	<div class='wrapper headline'>
		<div class='row'>
			<h2>Demnächst im Kino</h2>
		</div>
	</div>
	<div class='wrapper movie'>
		<div class='row'>
			{foreach $futureMovies val}
			<a href='?p=moviedetail&filmid={$val.id}'>
			<div class='col-xxs-12 col-xs-6 col-sm-4 col-md-4 shadow'>
				<img src='https://image.tmdb.org/t/p/w500{$val.poster}' title='{$val.title}' alt='{$val.title}' />
				<div class='inner'>
					<h3>{$val.title}</h3>
					<p><small>{$val.tagline}</small></p>
					<p>{truncate($val.discription 100)}</p>
					<p><span class='filmstart'>Filmstart: {date_format $val.release_date "%d.%m.%Y"}</span></p>
				</div>
			</div>
			</a>
			{/foreach}
			<!-- <div class='col-xs-12 col-sm-4 shadow'>
				<img src='media/movie/spiderman.jpg' title='Spiderman - Homecoming' alt='Spiderman - Homecoming' />
				<div class='inner'>
					<h3>Spiderman</h3>
					<p><small>Homecoming</small></p>
					<p>Nach seinem Aufeinandertreffen mit den Avengers ist Peter Parker alias Spider-Man wieder zurück </p>
					<p><span class='filmstart'>Filmstart: 04.04.2018</span></p>
				</div>
			</div>
			<div class='col-xs-12 col-sm-4 shadow'>
				<img src='media/movie/mordimorientexpress.jpg' title='Mord im Orient Express' alt='Mord im Orient Express' />
				<div class='inner'>
					<h3>Mord im Orient Express</h3>
					<p>Ein Passagier wird ermordet und damit ist klar, dass einer der übrigen Reisenden der Täter sein muss. </p>
					<p><span class='filmstart'>Filmstart: 04.05.2018</span></p>
				</div>
			</div> -->
		</div>
	</div>
</div>
{/block}
