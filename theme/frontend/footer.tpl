<footer>
  <div class='wrapper'>
    <div class='row'>
      <div class='col-xs-12 col-sm-6'>
        <p><b>KinoDB </b>ist eine innovative Kinoplattform, bei der man Spaß am Ticket kaufen hat. Entstanden ist dieses Projekt im Rahmen der Diplomarbeit des JWE Lehrgangs 2018 des WIFI der Wirtschaftskammer Salzburg.</p>
        <address>
          <b>Andreas Ruprecht</b><br />
          Klessheimer Allee 59b/4<br />
          5020 Salzburg<br />
          <a href='tel:+436763921500'>+43 676 39 21 500</a><br />
          <a href='mailto:kinodb@ruprecht.cc?subject=Anfrage'>kinodb@ruprecht.cc</a><br />
        </address>
      </div>
      <div class='col-xs-12 col-sm-6'>
        <ul class='text-right'>
          <li><a href='#'>Impressum</a></li>
          <li><a href='#'>Kontakt</a></li>
          <li><a href='#'>Datenschutzhinweis</a></li>

        </ul>
      </div>
    </div>
  </div>

  <div class='dark-footer'>
    <div class='wrapper'>
      <div class='row'>
        <div class='col-xs-12 col-sm-6'>
          &copy; 2018 | Andreas Ruprecht
        </div>
        <div class='col-xs-12 col-sm-6 text-right'>
          <span class='icon-facebook2'></span>  <span class='icon-twitter'></span>  <span class='icon-youtube'></span>
        </div>
      </div>
    </div>
  </div>
</footer>

</body>
</html>
