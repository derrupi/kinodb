{extends "index.tpl"}
{block "content"}
<div id='filmdetail'>
  <div class='wrapper'>
    <div class='row'>
      <div class='image-inner'>
        <div class='poster'>
          <img src='https://image.tmdb.org/t/p/w500{$movie.poster_path}' width="300" height="450" />
        </div>
        <div class='header-poster'>
          <div class='filmtitel'>
            <h1>{$movie.title}</h1>
          </div>
          <div class='icons'>
            <div class="tooltip">
              <img src="asset/filmdetails/herz.png" onmouseover="this.src='asset/filmdetails/herz_gefuellt.png'" onmouseout="this.src='asset/filmdetails/herz.png'"><span class="tooltiptext">Auf Merkliste speichern</span>
            </div>
            <div class="tooltip">
              <img src="asset/filmdetails/erinnerung.png" onmouseover="this.src='asset/filmdetails/erinnerung_gefuellt.png'" onmouseout="this.src='asset/filmdetails/erinnerung.png'"><span class="tooltiptext">An Filmstart erinnern</span>
            </div>
            <div class="tooltip">
              <img src="asset/filmdetails/play.png" onmouseover="this.src='asset/filmdetails/play_gefuellt.png'" onmouseout="this.src='asset/filmdetails/play.png'" id="youtubeplayer"><span class="tooltiptext">Trailer abspielen</span>
            </div>
          </div>
          <div class='infobox'>
            <h3>Handlung: </h3>
            <div class='infobox-detail'>
              <p>
                {$movie.overview}
              </p>
            </div>
            <div class='infobox-zusatz'>
              <p>
                {loop $genres} {$name} {/loop} | Dauer: {$movie.runtime} Min. | Ab 12 Jahren
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="lightbox">
    <div class="content">
      <div class="plyr__video-embed" id="player">
          <iframe class="youtube-video" src="https://www.youtube.com/embed/{$movie.trailer}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>
  <div class='wrapper'>
    <div class='row'>
      <div class='col-xs-12' id="moviedetailshows">
        <div>
          <h3>Alle Vorstellungen von <em>{$movie.title}</em> in der Übersicht</h3>
        </div>
          {foreach $show show}
          <div>
            <ul>
              <li>
                <a href='?p=ticketsdetail&id={$movie.id}&show={$show.id}'>{date_format $show.datum "%d.%m.%Y"} - {date_format $show.uhrzeit "%H:%M"} Uhr - Kino {$show.saalnr}</a>
              </li>
            </ul>
          </div>
          {/foreach}
        </div>
    </div>
  </div>



<script src="https://cdn.plyr.io/3.3.9/plyr.js"></script>
{/block}
