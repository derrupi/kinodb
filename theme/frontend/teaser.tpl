<div id='teaser'>
  <img src='media/teaser/starwars_teaser2.jpg' title='Star Wars: Die letzten Jedi' alt='Star Wars: Die letzten Jedi' />
  <div class='wrapper'>
    <div class='offset'>
      <p><small></small></p>
      <h2><span class='uppercase'>Star Wars:</span></h2>
      <p>Star Wars: Die letzten Jedi, Buch und Regie von Rian Johnson, setzt die Handlung von "Star Wars: Das Erwachen der Macht" rund um die Familie Skywalker fort.</p>
      <p><button class='tickets'>Tickets kaufen!</button></p>
    </div>
  </div>
</div>
</header>
