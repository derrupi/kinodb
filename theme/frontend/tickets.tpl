{extends "index.tpl"}
  {block "content"}

  <div id="tickets">
    <div class='wrapper '>
      <div class='row'>
        <div class="headline">
          <h1>Reservierungen</h1>
        </div>
        <div>
          <p>
            Hier sehen Sie alle Ihre aktuellen Reservierungen
          </p>
        </div>
      </div>
      <div>
        {if !empty($error)}
          {$error}
        {/if}
      </div>
      <div class="row">
        <table id='ticketsOverview'>
          <tr>
            <td>Film</td>
            <td>Vorstellung</td>
            <td>Tickets</td>
            <td>Reservierungscode</td>
            <td>Optionen</td>
            <td>Drucken</td>
          </tr>
          {loop $daten}
          <tr>
            <td>{$title}</td>
            <td>{date_format $vorstellungDatum "%d.%m.%Y"} - {date_format $vorstellungUhrzeit "%H:%M"} Uhr</td>
            <td>{$tickets}</td>
            <td>{$reservierungscode}</td>
            <td>{if $status==0}<a href="?p=tickets&action=delete&reservierungscode={$reservierungscode}">[Stornieren]</a>{else} Tickets wurde bereits abgeholt {/if}</td>
            <td><a href="?p=tickets&action=print&reservierungscode={$reservierungscode}" target="_blank">Drucken</a></td>
          </tr>
          {/loop}
        </table>
      </div>
    </div>
  </div>
  {/block}
