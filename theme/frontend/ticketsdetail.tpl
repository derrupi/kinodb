{extends "index.tpl"}

{block "content"}
<div id='booking'>
  <div class='wrapper'>
    <div class='row'>
      <div class='col-xs-12'>
        <div id='ticketsdetail-header'>
          {$movie.title}
        </div>
      </div>
    </div>
    {if $user.status=="ja"}
    <div id='saalplan_detail'>

      {for i 1 $theater.reihen}
        <div class='row'>
          {assign array('' 'a' 'b' 'c' 'd' 'e' 'f' 'g' 'h' 'i' 'j' 'k' 'l' 'm' 'n' 'o' 'p'  'q' 'r' 's' 't' 'u' 'v' 'w' 'x' 'y' 'z') sitzreihe}
          <div class='reihenbeschriftung'>
            <p class='seatText'>{upper($sitzreihe.$i)}</p>
          </div>
            {for x 1 $theater.sitzplaetze}
              <div class='singleSeat'>
                <label class="container">
                  <input class="seat " {if in_array("$i-$x", $resSeats)}class="res"{/if} name="{upper($sitzreihe.$i)}-{$x}" value='{upper($sitzreihe.$i)}-{$x}' type="checkbox"  {if in_array("$i-$x", $resSeats)}checked disabled{/if} />
                  <span class="checkmark"><p class='seatText'>{$x}</p></span>
                </label>
              </div>
            {/for}
          </div>
        {/for}
      </div>
      <div id='reservation'>
        <form method="post" action='?p=booking' name='reservation'>
          <h3>Ausgewählte Sitze:</h3>
          <p class='sitze'></p>
            <input type='hidden' name='show' value="{$show}" />
            <input type='hidden' name='resSeats' id='resSeats' value=''/>
            <input type='hidden' name='kunde' id='kunde' value='{$user.details.id}'/>
            <input type="input" name="userVorname" value="{$user.details.vorname}" /><br />
            <input type="input" name="userNachname" value="{$user.details.nachname}" /><br />
            <input type="input" name="userEmail" value="{$user.details.email}" /><br />
            <input type='submit' value='abschicken' />
        </form>
      </div>

      {else}

      <div>
        Bitte melde dich an um Kinokarten zu reservieren. <a href='?p=userlogin'>Hier anmelden</a>
      </div>
      {/if}
  </div>
</div>
{/block}
