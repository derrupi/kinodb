{extends "index.tpl"}

{block "content"}

<div id='programm'>
	<div class='wrapper headline'>
		<div class='row'>
			<h2>Unser Kinoprogramm für
				<form method="get" name="filmeDatum" action="">
					<input type="hidden" name="p" value="filme" />
					<select name="datum" onchange="document.forms.filmeDatum.submit()">
						<option value="{date_format $datum "%d.%m.%Y"}">{date_format $date "%d.%m.%Y"}</option>
						{for i 0 7}
							{assign math("$date + 86400") newDate}
							<option value="{date_format $newDate "%d.%m.%Y"}" {if $newDate == $sDate}selected{/if}>
								{date_format $newDate "%d.%m.%Y"}
							</option>
							{assign $newDate date}
						{/for}
					</select>
				</form></h2>
		</div>
	</div>
	{include(file='filmeOverview.tpl')}
    </div>
	</div>
{/block}
