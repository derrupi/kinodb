{extends "index.tpl"}

{block "content"}
<div id='userlogin'>
  <div class='wrapper'>



    <div class='row'>
      <p>
        {foreach $error val}
          {if $val.status == "0"}
            <li>
              {$val.error}
            </li>
          {/if}
        {/foreach}
      </p>
    </div>


    <div class='row'>
      <div class='col-xs-12 col-sm-6 usercontainer'>
        {include(file='userLoginRegister.tpl')}
      </div>

      <div class='col-xs-12 col-sm-6 usercontainer'>
        <div>
          {include(file='userLoginForm.tpl')}
        </div>
        <div>
          {include(file='userForgotPassword.tpl')}
        </div>
      </div>
    </div>
  </div>
</div>

{/block}
