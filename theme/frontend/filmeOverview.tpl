<div class='wrapper kinoprogramm'>
  <div class='row'>
    {foreach $movies movie}
          <div class='row'>
            <div class='detailview'>
              <div class='col-xxs-4 col-xs-3 col-sm-1 poster'>
                <img src='https://image.tmdb.org/t/p/w500{$movie.poster_path}' alt='{$movie.title}' title='{$movie.title}' />
              </div>
              <div class='col-xxs-8 col-xs-9 col-sm-11 infobox'>
              <div class='detailinfo'>
                <h3>{$movie.title}</h3>
              </div>
              <div>
                <p>
                  {foreach $movie.genres genres}
                    {if $.foreach.genres.last}
                      {$genres.name}
                    {else}
                      {$genres.name},
                    {/if}
                  {/foreach}
                  | Dauer: {$movie.runtime} Min. | Ab 12 Jahren
                </p>
              </div>
              <div>
                <p>
                  <i>Filmstart: </i>{date_format $movie.release_date "%d.%m.%Y"}
                </p>
              </div>
              <div>
                <a href='?p=moviedetail&filmid={$movie.id}' class='button'>Weitere Infos zum Film</a>
              </div>
            </div>

            <div class='col-xs-12 col-sm-4 anfangszeiten'>
                {foreach $movie.show show}
                <a href='?p=ticketsdetail&id={$movie.id}&show={$show.id}'>
                  <div class='anfangszeitdetail'>
                      <p><strong>{date_format $show.uhrzeit "%H:%M"}</strong><span><img src='asset/tickets/tickets_nichtverfuegbar.png' alt='Tickets kaufen' title='Tickets kaufen' /></span></p>
                      <p>Kino {$show.saalnr}</p>
                  </div>
                </a>
                {/foreach}
              </div>
              </div>
            </div>
          {/foreach}

  </div>
</div>
