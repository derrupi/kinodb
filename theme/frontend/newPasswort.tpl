{extends index.tpl}

{block "content"}
<div id='userlogin'>
  <div class='wrapper'>
    <div class='row'>
      <p>
        {foreach $error val}
          {if $val.status == "0"}
            <li>
              {$val.error}
            </li>
          {/if}
        {/foreach}
      </p>
    </div>
    <div class='row'>
      <div class='col-xs-12 usercontainer'>
        <form method="post" name="newPasswort" action="">
        <input type="hidden" name="code" value='{$code}'/>
        <h2>Neues Passwort setzen</h2>
        <div>
          <label for='passwort'>Passwort:</label><br />
          <input type='password' name='passwort' id='passwort' value=''/>
        </div>
        <div>
          <label for='passwortValidate'>Passwort bestätigen:</label><br />
          <input type='password' name='passwortValidate' id='passwortValidate' value='' />
        </div>
        <div>
          <input type='submit' name='submit' value='Neues Passwort setzen' />
        </div>
      </form>
    </div>

    </div>
  </div>
</div>
{/block}
