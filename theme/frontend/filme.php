<div id='filmverwaltung'>
  <div>
    <p>
      Hier können die aktuellen Filme verwaltet werden und neue Filme einfach angelegt werden.
    </p>
  </div>
  <div>
    <p>
      <a href='?p=film_anlegen&amp;action=neu' class='btn'>Neuen Film anlegen</a>
    </p>
  </div>
  <div class='filmliste'>
    <table>
      <thead>
        <th>Filmnummer</th>
        <th>Titel</th>
        <th>Laufzeit</th>
        <th>Filmstart</th>
        <th>Optionen</th>
      </thead>
<?php

  $sql = "SELECT * FROM filme ORDER by release_date";

  $db = db_datenbank::get_instanz();
  $ergebnis = $db->query($sql);

  while($row = mysqli_fetch_assoc($ergebnis)){
    echo "<tr>";
      echo "<td>{$row['id']}</td>";
      echo "<td>{$row['title']}</td>";
      echo "<td>{$row['runtime']}</td>";
      echo "<td>".transformDate($row['release_date'])."</td>";
      echo "<td></td>";
    echo "</tr>";
  }

 ?>
    </table>
  </div>
</div>
