{extends index.tpl}

  {block "content"}
    <div id='booking'>
      <div class='wrapper'>
        <div class='row'>
          <div id='title'>
            <h3>Vielen Dank!</h3>
          </div>
          <div>
            <p>
              Wir haben Ihre Reservierung erhalten und freuen uns auf Ihren Besuch. <br /><br />
              Hier Reservierung können Sie jederzeit <a href='?p=booking'>hier bearbeiten.</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  {/block}
