<!doctype html>
<html lang='de'>
{block "header"}
	<head>
		<title>Kino DB</title>
		<meta charset='utf-8' />
		<meta name="viewport" content="width=device-width; initial-scale=1.0;" />  
		<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
		<![endif]-->
		{block "header-url"}
	    <link href="https://fonts.googleapis.com/css?family=Raleway:400,600" rel="stylesheet">
			<link href='css/style.min.css' rel='stylesheet' type='text/css' />
			<link href='css/theme.min.css' rel='stylesheet' type='text/css' />
			<link rel="stylesheet" href="https://cdn.plyr.io/3.3.9/plyr.css">

		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		  <script src="js/frontend/index.js"></script>

		{/block}
{/block}
</head>
	<body>
    <header>
      <div class='wrapper'>
        <div class='row'>
          <div id='logo' class='col-xs-3 col-sm-4'>
            <a href='?=home'><img src='asset/png/logo_kinodb.png' title='Kino DB' alt='Logo Kino DB'/></a>
          </div>
          <div class='col-xs-9 col-sm-8'>
						<div>
	            <nav id='main'>
	              <ul>
									{loop $menue_type_list}
										{if $visible==1}
	                		<li><a href='?p={$url}'>{$name}</a></li>
										{/if}
	                {/loop}
	              </ul>
	            </nav>
						</div>
          </div>
					<div class='col-xs-9 col-sm-8'>
						<div class="dropdown">
							<div class="burgermenu" onclick="myFunction(this)">
							  <div class="bar1"></div>
							  <div class="bar2"></div>
							  <div class="bar3"></div>
							</div>
							<div id="myDropdown" class="dropdown-content">
								{loop $menue_type_list}{if $visible==1}<a href="?p={$url}">{$name}</a>{/if}{/loop}
						  </div>
						</div>
					</div>
				</div>
      </div>
