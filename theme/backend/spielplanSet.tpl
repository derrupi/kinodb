{extends "index.tpl"}

{block "content"}

  {assign array("SO", "MO", "DI", "MI", "DO", "FR", "SA") wochentage}
<div class='wrapper'>
  <div class='row'>
    <p>
      Vorstellungen für <b>{$spielplan.title}</b> anlegen
    </p>
  </div>
</div>
{assign $selected_kino kinonr}
<form method="post" name="kino" action="?p=spielplanDetail&view=backend&action=neu">
  <div class='wrapper'>
    <div class='row'>
      <p>
        Kinosaal:
        <select name="kino" onchange="document.forms.kino.submit()">

          {foreach $kino k}
            <option value="{$k.id}" {if $k.id == $kinonr}selected{/if}>Kino {$k.saalnr} (ID: {$k.id})</option>
          {/foreach}
        </select>
        <input type="hidden" name="filmid" value="{$filmid}" />
      </p>
    </div>
  </div>
</form>
<form method="post" name="kino" action="?p=spielplanDetail&view=backend&action=speichern">
  <input type="hidden" name="kino" value="{$selected_kino}" />
  <div class='dates'>

  {for i 0 $gesamtTage}
    {assign math("$startdatum + a*$i" a=86400) zeit}

    <div class='datecontainer'>
      <div class='date-title'>
        <div class='weekday'>
          {date_format $zeit "%a"}
        </div>
        <div class='date'>
          {date_format $zeit "%d.%m.%Y"}
        </div>
      </div>
      {assign "14:30" time}
    {for x 0 20}
      {assign Add30min($time) newTime}
      <div class='time' id='{$i}-{$x}'>
        {assign "$i-$x" input}
        <label><input type='checkbox' name="spielzeit[{$i}-{$x}]" class='spielplan_chechbox'
          {if InArray($input, $vorstellungen)}checked{/if}
           id='{$i}-{$x}'/>
          {$newTime}
        </label>
      </div>
      {assign $newTime time}
    {/for}
    </div>

  {/for}
  </div>
  <div>
    <input type="hidden" name="filmid" value="{$spielplan.id}" />
    <input type="submit" name="form" value="Speichern" />
  </div>
</form>

{/block}
