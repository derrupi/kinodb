{extends "index.tpl"}

{block "content"}
<div class='row'>
  <p>
    {foreach $error val}
      {if $val.status == "0"}
      <div class='errors'>
        <li>
          {$val.error}
        </li>
        </div>
      {/if}
    {/foreach}
  </p>
</div>
{if $error.status != 1}
<div>
  <form method="post" name="userlogin" action="?p=login&view=backend&action=login">
  <input type="hidden" name="userform" value='2'/>
  <h2>Userlogin</h2>
    <div>
      <label for='email'>Email:</label><br />
      <input type='text' name='email' id='email' value='{if is_array($error.emailLogin) or empty($error.emailLogin)}{else}{$error.emailLogin}{/if}'/>
    </div>
    <div>
      <label for='passwort'>Passwort:</label><br />
      <input type='password' name='passwort' id='passwort' value='{if is_array($error.passwort) or empty($error.passwort)}{else}{$error.passwort}{/if}'/>
    </div>
    <div>
      <input type='submit' name='submit2' value='Benutzer einloggen' />
    </div>
  </form>
</div>
{/if}
{/block}
