{extends "index.tpl"}

{block "content"}
  <div id='vorstellungen'>
    <p>
      Hier werden alle Vorstellungen von {$movie.title} angezeigt und es können die Vorstellungspreise verändert werden.</a>
    </p>
    <div>
      <form method="post" name="shows" action="?p=showDetail&view=backend&action=speichern">
        <input type="hidden" name="filmid" value="{$filmid}" />
        <table>
          <tr>
            <th>Vorstellungsid</th>
            <th>Datum</th>
            <th>Uhrzeit</th>
            <th>Preis</th>
          </tr>
        </table>
        {foreach $shows show}
        <tr>
          <td><input type="hidden" size="10" name="vorstellungsid[]" value="{$show.id}" /></td>
          <td><input type="date" name="datum[]" value="{$show.datum}" disabled/></td>
          <td><input type="text" name="uhrzeit[]" value="{$show.uhrzeit}" disabled/></td>
          <td><input type="text" name="preis[]" value="{$show.preis}"/></td>
        </tr>
        {/foreach}
        <tr>
          <td>
            <input type="submit" name="shows" value="Preise speichern" />
          </td>
        </tr>
      </form>

    </div>
  </div>
{/block}
