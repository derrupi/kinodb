{extends "index.tpl"}

{block "content"}
<div id='laufzeitverwaltung'>
  <div>
    <p>
      Hier können Sie die Laufzeiten der Filme festlegen die Sie zeigen möchten..
    </p>
  </div>
  <div class='filmliste'>
    <table>
      <thead>
        <th>Filmnummer</th>
        <th>Titel</th>
        <th>Releasedate</th>
        <th>Laufzeit anlegen</th>
      </thead>

    {foreach $movies val}
    <tr>
      <td>{$val.id}</td>
      <td>{$val.title}</td>
      <td>{$val.release_date}</td>
      <td><a href="?p=laufzeit&view=backend&filmid={$val.id}">Laufzeit anlegen</a></td>
  </tr>
  {/foreach}
    </table>
  </div>
</div>
{/block}
