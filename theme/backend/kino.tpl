{extends "index.tpl"}

{block "content"}
<div id="kinoverwaltung">
  <p>
    Hier können die Kinosäle verwaltet werden und <a href="?p=kinoNew&view=backend">neue Kinosäle angelegt werden</a>.
  </p>
  <table>
    <thead>
      <th>Saal-ID</th>
      <th>Saalnummer</th>
      <th>Sitzplätze pro Reihe</th>
      <th>Reihen</th>
      <th>Gesamtsitzplätze</th>
      <th>Optionen</th>
    </thead>
    {loop $kinos}
      <tr>
        <td>{$id}</td>
        <td>{$saalnr}</td>
        <td>{$sitzplaetze}</td>
        <td>{$reihen}</td>
        <td>{math "$sitzplaetze*$reihen"}</td>
        <td><a href='?p=kinoEdit&view=backend&kino={$id}'>Bearbeiten</a></td>
      </tr>
    {/loop}
  </table>
</div>


{/block}
