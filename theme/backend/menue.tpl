<!doctype html>
<html lang='de'>
	<head>
		<title>KinoDB Backend</title>
		<meta charset='utf-8' />
		<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
		<![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="js/backend/datepicker-config.js"></script>
		<script src="js/backend/spielplanSet.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,700" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link href='css/backend/backendMain.min.css' rel='stylesheet' type='text/css' />
	</head>
  <body>
    <header>
      <nav>
        <ul>
         <li><a href="?p=home"><i class="fa fa-home"></i> Home</a></li>
				 <li><a href="?p=benutzer&view=backend"><i class="fa fa-user"></i> Kunden</a></li>
         <li><a href="?p=tickets&view=backend"><i class="fa fa-ticket"></i> Tickets</a></li>
				 <li><a href="?p=kino&view=backend"><i class="fa fa-play"></i> Kinosäle</a></li>
         <li><a href="?p=filme&view=backend"><i class="fa fa-film"></i> Filme</a></li>
         <li><a href="?p=spielplan&view=backend"><i class="fa fa-calendar"></i> Spielplan</a></li>
				 <li><a href="?p=settings&view=backend"><i class="fa fa-edit"></i> Einstellungen</a></li>
         <li><a href="?p=logout&view=backend"><i class="fa fa-close"></i> Logout</a></li>
        </ul>
      </nav>
    </header>

    <div id='content'>
