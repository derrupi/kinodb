{extends "index.tpl"}

{block "content"}
<div id="settings">
  <p>
    Hier werden die Grundeinstellungen der KINODB konfiguriert
  </p>
  {with $daten}
  <form method="post" action="?p=settings&view=backend&action=speichern">
    <div>
      <label for="grundpreis">Grundpreis pro Karte: </label><br />
      <input type="text" id="grundpreis" name="grundpreis" value="{$grundpreis}"/>
    </div>
    <div>
      <input type="submit" name="submit" value="Einstellungen speichern" />
    </div>
  </form>
  {/with}
</div>


{/block}
