{extends "index.tpl"}

{block "content"}
  <div id='spielplan'>
    <p>
      Hier wird der Spielplan definiert. Damit ein Spielplan definiert werden kann, muss jeder <a href='?p=laufzeitIndex&view=backend'>Film eine Laufzeit</a> haben. <a href='?p=laufzeitIndex&view=backend'>[Laufzeit anlegen]</a>
    </p>

<table>
    <thead>
    <th>Filmid</th>
    <th>Titel</th>
    <th>Releasedate</th>
    <th>Startdatum</th>
    <th>Enddatum</th>
    <th>Spielplan </th>
    <th>Vorstellungen</th>
    </thead>
      {loop $movies}
        <tr>
          <td>{$id}</td>
          <td>{$title}</td>
          <td>{date_format $release_date "%d.%m.%Y"}</td>
          <td>{date_format $startdatum "%d.%m.%Y"}</td>
          <td>{date_format $enddatum "%d.%m.%Y"}</td>
          <td><a href="?p=spielplanDetail&view=backend&action=neu&filmid={$id}">Spielplan anlegen</a></td>
          <td><a href="?p=showDetail&view=backend&filmid={$id}">Vorstellungen anzeigen</a></td>
        </tr>
      {/loop}
    </table>

  </div>
{/block}
