{extends "index.tpl"}

{block "content"}
{if $error == 1}
  <b>Der Film wurde gespeichert!</b>
{else}
  {if !empty($error)}
    <div class='errors'>
    {loop $error}
      <li>
        {$error}
      </li>
    {/loop}
    </div>
  {/if}
<form method='post' action='?p=neuerfilm2&view=backend&action=speichern'>
  <input type='hidden' name='moviedb_id' value='{$film.filmid}'>
<div id='filmdetailansicht'>
    <div class='poster'>
      <input type='hidden' name='poster_path' value='{$film.poster_path}' />
        <img src='https://image.tmdb.org/t/p/w500{$film.poster_path}' width='350px'>
    </div>
    <div class='filmdetails_important'>
      <label for="title">Filmtitel*</label><br />
      <input data-validation="length" data-validation-length="max40" type="text" id="title" name="title" value="{$film.title}"><br />
      <label for="tagline">Tagline</label><br />
      <input type="text" id="tagline" name="tagline" value="{$film.tagline}"><br />
      <label for="original_language">Originalsprache</label><br />
      <input type="text" id="original_language" name="original_language" value="{$film.original_language}"> <br />
      <label for="original_title">Originaltitel</label><br />
      <input type="text" id="original_title" name="original_title" value="{$film.original_title}"> <br />
      <label for="release_date">Veröffentlichung*</label><br />
      <input type="text" id="release_date" name="release_date" value="{date_format $film.release_date "%d.%m.%Y"}"> <br />
      <label for="runtime">Laufzeit (in Minuten)*</label><br />
      <input type="text" id="runtime" name="runtime" value="{$film.runtime}"> <br />
      <label for="production_companies">Produktionsfirmen</label><br />
      <input type="text" id="production_companies" name="production_companies" value="{loop $film.production_companies}{$name} {/loop}"><br />
      <label for="trailer">Trailer auf YouTube [<a href='https://www.youtube.com/watch?v={$trailer}' target='_blank'>Schauen</a>]</label><br />
      <input type="text" id="trailer" name="trailer" value="{$trailer}"><br />
    </div>
    <div class='filmdetails'>
      <label for="genres">Genres*</label><br />
      <input type="text" id="genres" name="genres" value="{loop $film.genres}{$name} {/loop}"><br />
      <label for="budget">Produktionsbudget (in US-Dollar)</label><br />
      <input type="text" id="budget" name="budget" value="{$film.budget}"> <br />
      <label for="homepage">Website</label><br />
      <input type="text" id="homepage" name="homepage" value="{$film.homepage}"><br />
      <label for="overview">Handlung*</label><br  />
      <textarea name='overview' id='overview' onkeydown="letter_counter()">{$film.overview}</textarea><br />
      <span id='letter'>Sie haben <span id='letter_counter'></span> Zeichen! Notwendig sind: 150</span>
    </div>

</div>
<input type="submit" value="Film anlegen" />
<script>
    window.onload = function () {
           console.log('Dokument geladen');
           document.getElementById("letter_counter").innerHTML = '{$OverviewCount}';
       }
    function letter_counter(){
      var minCount ='150';
      var count = document.getElementById("overview").value.length;
      document.getElementById("letter_counter").innerHTML = count;
        if(minCount < count){
          document.getElementById("letter").style.color="#008b45";
        } else {
          document.getElementById("letter").style.color="#000";
        }
    }
</script>
</form>
{/if}
{/block}
