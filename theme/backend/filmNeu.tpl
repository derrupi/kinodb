{extends "index.tpl"}

{block "content"}
<div id='neuerFilm'>

</div>

  {if !empty($filme)}
  <div>
    {if $count == 1}
      <p>
        Es gibt 1 Suchergebnis
      </p>
    {else}
      <p>
        Es gibt {$count} Suchergebnisse!
      </p>
    {/if}
      <table>
        {loop $filme}
          <tr>
            <td><a href="?p=neuerfilm2&view=backend&step=3&filmid={$id}">{$title} (Erstveröffentlichung: {date_format $release_date "%d.%m.%Y"})</a></td>
          </tr>
        {/loop}
      </table>
      <a href="?p=neuerfilm&view=backend">Anderen Film suchen</a>
    </div>
  {/if}
{if empty($filme)}
<div>
  <p>Hier können Sie einen neuen Film anlegen</p>
  <form method='post' action='?p=neuerfilm&view=backend&action=step2'>
    <label for="filmtitel">Filmtitel</label><br />
    <input data-validation="length" data-validation-length="max40" type="text" id="filmtitel" name="filmtitel"><br />
    <label for="methode">Methode</label><br />
    <select name='methode' id='methode'>
      <option value='0' selected>Manuell</option>
      <option value='1'>Automatisch</option>
    </select><br />
    <input type="submit" value="Film anlegen" />
  </form>
</div>
{/if}

{/block}
