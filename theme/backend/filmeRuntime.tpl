{include(file='menue.tpl')}
  {block "content"}
  <div>
    Laufzeit festlegen für <b>{$movies.title}</b>
  </div>
      <form method="post" action='?p=laufzeit&view=backend&action=set'>
        <input type='hidden' name='filmid' value='{$data.filmid}'>
        <div>
          <label for='fromdate'>Startdatum: </label><br />
          <input type='text' id='fromdate' name='fromdate' /><br />
        </div>
        <div>
          <label for='todate'>Enddatum: </label><br />
          <input type='text' id='todate' name='todate' /><br />
        </div>
        <div>
        <input type='submit' name='laufzeit' value='Laufzeit anlegen' />
        </div>
      </form>
  {/block}
