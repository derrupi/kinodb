{extends "index.tpl"}

{block "content"}
<div id="customer">
  <div class='row'>

      {foreach $error val}
        {if $val.status == "0"}
        <div class="errors">
          <li>
            {$val.error}
          </li>
          </div>
        {/if}
      {/foreach}
  </div>
  <p>
    In der Userverwaltung kann man User löschen, Passwörter zurücksetzen oder User entsperren.
  </p>
    <table>
      <thead>
        <th>Userid</th>
        <th>Vorname</th>
        <th>Nachname</th>
        <th>Email</th>
        <th>Last Login</th>
        <th>Kunden löschen</th>
        <th>Gesperrt?</th>
        <th>Neues Passwort zusenden</th>
      </thead>
      {foreach $users val}
      <tr>
        <td>{$val.id}</td>
        <td>{$val.vorname}</td>
        <td>{$val.nachname}</td>
        <td>{$val.email}</td>
        <td>{$val.lastLogin}</td>
        <td><a href='?p=benutzer&view=backend&action=delete&userid={$val.id}'>Löschen</a></td>
        <td>{if $val.gesperrt >= 5}<a href='?p=benutzer&view=backend&action=unlock&userid={$val.id}'>Entsperren</a>{else}{$val.gesperrt}{/if}
        </td>
        <td><a href='?p=benutzer&view=backend&action=newPasswort&userid={$val.id}'>Passwort senden</a></td>
      </tr>
      {/foreach}
    </table>
</div>
{/block}
