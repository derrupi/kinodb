{extends "index.tpl"}

{block "content"}
<div id='filmverwaltung'>
  <div>
    <p>
      Hier können die aktuellen Filme verwaltet werden und neue Filme einfach angelegt werden.
    </p>
  </div>
  <div>
    <p>
      <a href='?p=neuerfilm&view=backend' class='btn'>Neuen Film anlegen</a>
    </p>
  </div>
  <div class='filmliste'>
    <table>
      <thead>
        <th>Filmnummer</th>
        <th>Titel</th>
        <th>Laufzeit</th>
        <th>Filmstart</th>
        <th>Optionen</th>
      </thead>

    {foreach $movies val}
    <tr>
      <td>{$val.id}</td>
      <td>{$val.title}</td>
      <td>{$val.runtime}</td>
      <td>{$val.release_date}</td>
      <td></td>
  </tr>
  {/foreach}
    </table>
  </div>
</div>
{/block}
