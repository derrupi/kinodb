{extends "index.tpl"}

{block "content"}
<div id="kinoNew">
  <p>

  </p>
  {if empty($status) or $status === 0}
  <form method="post" action="?p=kinoNew&view=backend&action=speichern">
    <div>
      <label for="kinosaal">Kinonummer: </label><br />
      <input type="text" id="kinosaal" name="kinosaal" value="{$lastCinema}" readonly/>
    </div>
    <div>
      <label for="reihen">Reihen:</label><br />
      <select id="reihen" name="reihen">
        {for i 1 15}
          <option value="{$i}">{$i}</option>
        {/for}
      </select>
    </div>
    <div>
      <label for="sitze">Sitzplätze pro Reihen:</label><br />
      <select id="sitze" name="sitze">
        {for i 1 20}
          <option value="{$i}">{$i}</option>
        {/for}
      </select>
    </div>
    <div>
      <input type="submit" name="submit" value="Kinosaal anlegen" />
    </div>
  </form>
  {else}
    <p>
      Der neue Kinosaal wurde erfolgreich angemeldet.
    </p>
  {/if}
</div>

{/block}
