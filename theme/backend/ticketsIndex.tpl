{extends "index.tpl"}

{block "content"}
  <p>
    Hier können alle Reservierungen verwaltet werden für Vorstellungen von heute oder in der Zukunft.
  </p>
  <div>
    {if !empty($error)}
      {$error}
    {/if}
  </div>
  <div>
    <table width="70%">
      <thead>
        <tr>
          <th>Reservierungscode</th>
          <th>Anzahl der Tickets</th>
          <th>Vor- und Zuname</th>
          <th>E-Mail</th>
          <th>Vorstellungsdatum und Uhrzeit</th>
          <th>Optionen</th>
          <th>Status</th>
          <th>Drucken</th>
        </tr>
      </thead>
      <tbody>
        {loop $tickets}
        <tr>
          <td>{$reservierungscode}</td>
          <td>{$tickets}</td>
          <td>{$vorname} {$nachname}</td>
          <td>{$email}</td>
          <td>{date_format $vorstellungDatum "%d.%m.%Y"} - {date_format $vorstellungUhrzeit "%H:%M"} Uhr</td>
          <td><a href="?view=backend&p=tickets&action=edit&reservierungscode={$reservierungscode}">[Ändern]</a> <a href="?view=backend&p=tickets&action=delete&reservierungscode={$reservierungscode}">[Löschen]</a></td>
          <td>{if $status==0}<a href="?view=backend&p=tickets&action=status&reservierungscode={$reservierungscode}">[Als abgeholt markieren] {else} Tickets bereits abgeholt {/if}</a></td>
          <td><a href="?view=backend&p=tickets&action=print&reservierungscode={$reservierungscode}" target="_blank">[Drucken]</a></td>
        </tr>
        {/loop}

      </tbody>
    </table>
  </div>
{/block}
