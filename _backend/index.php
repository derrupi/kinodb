<?php
include_once '../inc/functions.inc.php';
sec_session_start();

// if(checkLogin()){
//   header('Location: index.php?p=login');
// }
?>
<!doctype html>
<html lang='de'>
	<head>
		<title>Kino DB</title>
		<meta charset='utf-8' />
		<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
		<![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="js/datepicker-config.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,700" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link href='style.min.css' rel='stylesheet' type='text/css' />
	</head>
  <body>
    <header>
      <nav>
        <ul>
         <li><a href="?p=home"><i class="fa fa-home"></i> Home</a></li>
				 <li><a href="?p=tickets"><i class="fa fa-user"></i> Kunden</a></li>
         <li><a href="?p=tickets"><i class="fa fa-ticket"></i> Tickets</a></li>
         <li><a href="?p=filme"><i class="fa fa-film"></i> Filme</a></li>
         <li><a href="?p=spielplan"><i class="fa fa-calendar"></i> Spielplan</a></li>
         <li><a href="?p=logout"><i class="fa fa-close"></i> Logout</a></li>
        </ul>
      </nav>
    </header>

    <div id='content'>
      <?php
      // Überprüft welche Seite ausgewählt wurde, wenn keine ausgewählt wurde wird home ausgewählt
      (empty($_GET['p'])) ? $p = 'home.php': $p = $_GET['p'].".php";

      if(file_exists('content/'.$p)){
        include_once 'content/'.$p;
      }
       ?>
    </div>
  </body>
</html>
