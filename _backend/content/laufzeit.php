<div id='runtime'>
  <p>
    Hier können Sie die Laufzeiten der Filme festlegen die Sie zeigen möchten.
  </p>

  <?php
$errors = array();
$success = array();

$sqlGet = db_datenbank::get_instanz();
$sql_get = $sqlGet->escape($_GET);

$action = (!empty($sql_get['action'])) ? $sql_get['action'] : '';
if($action==""){
  $sql = "SELECT * FROM filme
    WHERE NOT EXISTS (SELECT id FROM laufzeit WHERE film = filme.id)";

  $db = db_datenbank::get_instanz();
  $ergebnis = $db->query($sql);

  echo "<table>";
  echo "<thead>";
  echo "<th>Filmid</th>";
  echo "<th>Titel</th>";
  echo "<th>Releasedate</th>";
  echo "<th>Laufzeit anlegen</th>";
  echo "</thead>";
  while($row = mysqli_fetch_assoc($ergebnis)){
    echo "<tr>";
    echo "<td>{$row['id']}</td>";
    echo "<td>{$row['title']}</td>";
    echo "<td>".transformDate($row['release_date'])."</td>";
    echo "<td><a href='?p=laufzeit&action=neu&filmid={$row['id']}'>Laufzeit anlegen</a></td>";
    echo "</tr>";
  }
  echo "</table>";
} elseif ($sql_get['action']=="neu") {
if(!empty($_POST)){

  $sqlPost = db_datenbank::get_instanz();
  $sql_post = $sqlPost->escape($_POST);

  $datensatz = array();
  if(!empty($sql_post['fromdate'])){
      if (!preg_match('/[\d]{2}+[\.]+[\d]{2}+[\.]+[\d]{4}/', $sql_post['fromdate']))  {
          $errors[] = 'Bitte geben Sie das Startdatum der Laufzeit in einem validen Format an (tt.mm.yyyy)!';
      } else {
        $datensatz['startdatum'] = DateSQL($sql_post['fromdate']);
      }
    } else {
      $errors[] = 'Bitte definieren Sie ein Startdatum der Laufzeit';
    }

    if(!empty($sql_post['todate'])){
        if (!preg_match('/[\d]{2}+[\.]+[\d]{2}+[\.]+[\d]{4}/', $sql_post['todate']))  {
            $errors[] = 'Bitte geben Sie das Enddatum der Laufzeit in einem validen Format an (tt.mm.yyyy)!';
        } else {
          $datensatz['enddatum'] = DateSQL($sql_post['todate']);
        }
      } else {
        $errors[] = 'Bitte definieren Sie ein Enddatum der Laufzeit';
      }

  if(empty($errors)){
    $datensatz['film'] = $sql_post['filmid'];
    if($sqlPost->insert("laufzeit", $datensatz)){
      $success[] = "Laufzeit wurde angelegt.";
    }
  }
}
if(count($errors) >= 1){
  echo "<div class='errors'>";
  echo "<ul>";
  echo (count($errors) >= 3) ? 'Der hellste Stern am Himmel sind Sie wohl nicht, oder? Alle Felder die mit * markiert sind, sind Plfichtfelder' : '';
  foreach($errors as $error){
    echo "<li>".$error."</li>";
  }
  echo "</ul>";
  echo "</div>";
}

if(count($success) >= 1){
  echo "<div class='success'>";
  echo "<ul>";
  foreach($success as $erfolg){
    echo "<li>".$erfolg."</li>";
  }
  echo "</ul>";
  echo "</div>";
}
?>
<form method="post" action='?p=laufzeit&action=neu'>
  <input type='hidden' name='filmid' value='<?php echo $sql_get['filmid']; ?>'>
  <div>
    <label for='fromdate'>Startdatum: </label><br />
    <input type='text' id='fromdate' name='fromdate' /><br />
  </div>
  <div>
    <label for='todate'>Enddatum: </label><br />
    <input type='text' id='todate' name='todate' /><br />
  </div>
  <div>
  <input type='submit' name='laufzeit' value='Laufzeit anlegen' />
  </div>
</form>

<?php

}
  ?>
</div>
