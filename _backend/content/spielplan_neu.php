<?php
$wochentage = array("SO", "MO", "DI", "MI", "DO", "FR", "SA");
$startdatum = strtotime($ausgabe['startdatum']);
$enddatum = strtotime($ausgabe['enddatum']);

echo "<div class='dates'>";
for ($i = 0; $i < date("d", $enddatum - $startdatum); $i++) {
  $zeit = $startdatum + 86400*$i;
  echo "<div class='datecontainer'>";
  echo "<div class='date-title'>";
    echo "<div class='weekday'>";
    echo $wochentage[date("w", $zeit)];
    echo "</div>";
    echo "<div class='date'>";
    echo date("d.m.Y", $zeit);
    echo "</div>";
  echo "</div>";

  $time = '15:00';
  for ($x = 0; $x < 20; $x++) {
    $newTime = date('H:i',strtotime($time . ' +30 minutes'));
    echo "<div class='time' id='{$i}-{$x}'><label><input type='checkbox' class='spielplan_chechbox' id='{$i}-{$x}'/>{$newTime}</label></div>";
    $time = $newTime;
  }
  echo "</div>";
}
echo "</div>";

?>
