<?php
unset($_SESSION['formular']);
$_SESSION['formular'] = array();

$errors = array();

if(!empty($_POST)){
  foreach($_POST as $key => $value){
    if(empty($value) AND $key != 'methode') {
      $errors[] = "Das Feld ".ucfirst($key). "darf nicht leer sein";
    }
  }

  if(empty($errors)){
    $sqlPost = db_datenbank::get_instanz();
    $sql_post = $sqlPost->escape($_POST);

    if($sql_post['methode']=="1"){

          $movieRequest = new api_moviedb();
          $film = $movieRequest->searchMovie($sql_post['filmtitel']);

          $count = count($film['results']);
          $ergebnis = ($count > 1) ? " Ergebnisse" : " Ergebnis";

          if($count == 1){
            header("Location: index.php?p=film_anlegen2&filmid={$film['results'][0]['id']}");

          } elseif($count > 1){

            echo "Es gibt ".$count.$ergebnis." für deine Suche <br />";
            for ($i = 0; $i < $count; $i++) {
              echo "<a href='?p=film_anlegen2&amp;filmid={$film['results'][$i]['id']}'>".$film['results'][$i]['title']." (Release Date: ".transformDate($film['results'][$i]['release_date']).")</a><br />";
            }
          } else {
            echo "Es wurde leider kein Film gefunden";
          }
      } else {

        $_SESSION['formular'] = $_POST['filmtitel'];

        header("Location: index.php?p=film_anlegen2");
      }
  }
}
?>
<div id='neuerFilm'>
    <?php
    if(count($errors) >= 1){
      echo "<div class='errors'>";
      echo "<ul>";
      echo (count($errors) >= 3) ? 'Der hellste Stern am Himmel sind Sie wohl nicht, oder? Alle Felder die mit * markiert sind, sind Plfichtfelder' : '';
      foreach($errors as $error){
        echo "<li>".$error."</li>";
      }
      echo "</ul>";
      echo "</div>";
    }
    ?>
  </div>
  <form method='post' action='?p=film_anlegen'>
    <label for="filmtitel">Filmtitel</label><br />
    <input data-validation="length" data-validation-length="max40" type="text" id="filmtitel" name="filmtitel"><br />
    <label for="methode">Methode</label><br />
    <select name='methode' id='methode'>
      <option value='0' selected>Manuell</option>
      <option value='1'>Automatisch</option>
    </select><br />
    <input type="submit" value="Film anlegen" />
  </form>
</div>
