<?php
  $errors = array();
  $success = array();
  $film = '';
  $production_companies = '';
  $genres = '';
  $OverviewMinZeichen = 100;
  $OverviewCount = 0;

  // Hier validiere ich die Daten
  if(!empty($_POST)){
    $return = array();
    $sqlPost = db_datenbank::get_instanz();
    $sql_post = $sqlPost->escape($_POST);

    $return['moviedb_id'] = $sql_post['moviedb_id'];

    if (!empty($sql_post['title'])){
        if (!filter_var($sql_post['title'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW)) {
            $errors[] = 'Bitte geben Sie einen validen Filmtitel ein!';
        } else {
          $return['title'] = inputTest($sql_post['title']);
        }
    } else {
        $errors[] = 'Bitte geben Sie einen Filmtitel ein!';
    }

    if (!empty($sql_post['tagline'])){
        if (!filter_var($sql_post['tagline'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW)) {
            $errors[] = 'Bitte geben Sie eine validen Tagline ein!';
        } else {
          $return['tagline'] = inputTest($sql_post['tagline']);
        }
    }

    if (!empty($sql_post['poster_path'])){
        if (!filter_var($sql_post['poster_path'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW)) {
            $errors[] = 'Bitte geben Sie eine validen Tagline ein!';
        } else {
          $return['poster_path'] = inputTest($sql_post['poster_path']);
        }
    }

    if (!empty($sql_post['original_language'])){
        if (!filter_var($sql_post['original_language'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW)) {
            $errors[] = 'Bitte geben Sie die Originale Filmsprache in einem validen Format ein!';
        } else {
          $return['original_language'] = inputTest($sql_post['original_language']);
        }
    }

    if (!empty($sql_post['original_title'])){
        if (!filter_var($sql_post['original_title'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW)) {
            $errors[] = 'Bitte geben Sie den originalen Filmtitel in einem validen Format ein!';
        } else {
          $return['original_title'] = inputTest($sql_post['original_title']);
        }
    }

    if (!empty($sql_post['release_date'])){
        if (!preg_match('/[\d]{2}+[\.]+[\d]{2}+[\.]+[\d]{4}/', $sql_post['release_date']))  {
            $errors[] = 'Bitte geben Sie das Veröffentlichungsdatum in einen validen Format ein!';
        } else {
            $datum_jetzt = time();
            $datum_dann = explode(".",$sql_post['release_date']);
            $timestamp_dann = mktime(date("H"),date("i"),date("s"),$datum_dann[1],$datum_dann[0],$datum_dann[2]);
                if($datum_jetzt >= $timestamp_dann){
                    $errors[] = 'Datum liegt in der Vergangenheit! Film kann nur in der Zukunft gestartet werden!';
                } else {
                   $return['release_date'] = DateSQL(inputTest($sql_post['release_date']));
                }
        }
    } else {
        $errors[] = 'Bitte geben Sie an, wann der Film veröffentlicht wird!';
    }

    if (!empty($sql_post['runtime'])){
        if (!filter_var($sql_post['runtime'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW)) {
            $errors[] = 'Bitte geben Sie die Laufzeit in einen validen Format ein!';
        } else {
          if(strlen($sql_post['runtime']) <= 1){
            $errors[] = 'Der Film wird doch wohl länger dauern? Kurzfilme spielen wir nicht.';
          } else {
            $return['runtime'] = inputTest($sql_post['runtime']);
          }
        }
    } else {
        $errors[] = 'Bitte geben Sie eine Laufzeit für den Film an!';
    }

    // if (!empty($sql_post['production_companies'])){
    //     if (!filter_var($sql_post['production_companies'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW)) {
    //         $errors[] = 'Bitte geben Sie die Produktionsfirmen in einem validen Format ein!';
    //     } else {
    //       $return['production_companies'] = explode(",", inputTest($sql_post['production_companies']));
    //     }
    // }

    if (!empty($sql_post['genres'])){
        if (!filter_var($sql_post['genres'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW)) {
            $errors[] = 'Bitte geben Sie die Genres in einem validen Format ein!';
        } else {
          $returnGenres = explode(",", inputTest($sql_post['genres']));
        }
    } else {
        $errors[] = 'Sie müssen mindestens ein Genre eingeben!';
    }

    if (!empty($sql_post['budget'])){
        if (!filter_var($sql_post['budget'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW)) {
            $errors[] = 'Bitte geben Sie das Budget in einem validen Format ein!';
        } else {
          $return['budget'] = str_replace ( '.', '', inputTest($sql_post['budget']));
        }
    }

    if (!empty($sql_post['homepage'])){
        if (!filter_var($sql_post['homepage'], FILTER_VALIDATE_URL)) {
            $errors[] = 'Bitte geben Sie eine valide URL ein!';
        } else {
          $return['homepage'] = inputTest($sql_post['homepage']);
        }
    }

    if (!empty($sql_post['overview'])){
        if (!filter_var($sql_post['overview'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW)) {
            $errors[] = 'Bitte geben Sie einen validen Filmtitel ein!';
        } else {
          $OverviewCount = strlen($sql_post['overview']);
          if($OverviewCount <= $OverviewMinZeichen){
            $errors[] = "Bitte geben Sie für die Handlung mindestens {$OverviewMinZeichen} Zeichen ein!";
          } else {
            $return['overview'] = inputTest($sql_post['overview']);
          }

        }
    } else {
        $errors[] = "Bitte geben Sie eine Handlung ein! Mindestens {$OverviewMinZeichen} Zeichen.";
    }

    if(empty($errors)){
      // Film in Datenbank eintragen
      $db = db_datenbank::get_instanz();
      $sql = $db->query("SELECT id FROM filme WHERE title LIKE '%{$sql_post['title']}%'");
      // Kontrolle ob der Film bereits existiert - wir wollen schließlich keine Dupletten
      $count = mysqli_num_rows($sql);
      $row = mysqli_fetch_assoc($sql);
      if($count >= 1){
        $errors[] = "Der Film existiert bereits mit der ID {$row['id']}, wollen Sie den Film bearbeiten?";
      } else {
        // Wenn es den Film noch nicht gibt, wird er in die Datenbank eingetragen
          if($db->insert("filme", $return)){
              $ergebnis = mysqli_fetch_assoc($db->query("SELECT id FROM filme WHERE title LIKE '%{$sql_post['title']}%'"));
              foreach($returnGenres as $key){
                $key = trim($key);
                $row = mysqli_fetch_assoc($db->query("SELECT id FROM genres WHERE name = '$key'"));
                if(mysqli_num_rows($db->query("SELECT id FROM genres WHERE name = '$key'"))==1){

                  $db->query("INSERT INTO filme_genres (film, genre) VALUES ('$ergebnis[id]', '$row[id]')");
                } else {
                  if($db->query("INSERT INTO genres (name) VALUES('$key')")){
                    $id = mysqli_insert_id($db);
                    $db->query("INSERT INTO filme_genres (film, genre) VALUES ('$ergebnis[id]', '$id')");
                  }
                }
              }

            $success[] = "Film wurde angelegt!";
            unset($_POST);
          } else {
            $errors[] = "Der Film konnt nicht angelegt werden!";
          }
      }

    }

  }



  // Filminfos von TheMovieDB abrufen.
  $sqlGet = db_datenbank::get_instanz();
  $sql_get = $sqlGet->escape($_GET);

  if(!empty($sql_get['filmid'])){

    $movie = new api_moviedb();
    $film = $movie->movieRequest('details',$sql_get['filmid']);
    $cast = $movie->movieRequest('cast',$sql_get['filmid']);
    $trailers = $movie->movieRequest('trailers',$sql_get['filmid']);
    $images = $movie->movieRequest('images',$sql_get['filmid']);

    $OverviewCount = strlen($film['overview']);

    //Produktionsfirmen ausgeben
    foreach ($film['production_companies'] as $key => $value) {
      foreach($value as $item => $wert){
        if($item=="name"){
          $production_companies .= $wert.", ";
        }
      }
    }
    $production_companies = substr($production_companies, 0, -2);

    // Genres ausgeben
    foreach ($film['genres'] as $key => $value) {
      foreach($value as $item => $wert){
        if($item=="name"){
          $genres .= $wert.", ";
         }
      }
    }
  $genres = substr($genres, 0, -2);

  }



if(count($errors) >= 1){
  echo "<div class='errors'>";
  echo "<ul>";
  echo (count($errors) >= 3) ? 'Der hellste Stern am Himmel sind Sie wohl nicht, oder? Alle Felder die mit * markiert sind, sind Plfichtfelder' : '';
  foreach($errors as $error){
    echo "<li>".$error."</li>";
  }
  echo "</ul>";
  echo "</div>";
}

if(count($success) >= 1){
  echo "<div class='success'>";
  echo "<ul>";
  foreach($success as $erfolg){
    echo "<li>".$erfolg."</li>";
  }
  echo "</ul>";
  echo "</div>";
}

if(count($success) != 1){
?>
<form method='post' action='?p=film_anlegen2'>
  <input type='hidden' name='moviedb_id' value='<?php echo (!empty($filmid)) ? $filmid : ''; ?>'>
<div id='filmdetailansicht'>
    <div class='poster'>
      <?php
      if(!empty($film) || !empty($_POST['poster_path'])){
        $imageUrl = (!empty($film)) ? $film['poster_path'] : $_POST['poster_path'];
        echo "<input type='hidden' name='poster_path' value='{$imageUrl}' />";
        echo "<img src='https://image.tmdb.org/t/p/w500{$imageUrl}' width='350px'>";
      } else {
        echo "<div style='width:350px'><input type='file' name='posterupload' size='40'></div>";
      }
      ?>
    </div>
    <div class='filmdetails_important'>
      <label for="title">Filmtitel*</label><br />
      <input data-validation="length" data-validation-length="max40" type="text" id="title" name="title" value="<?php echo htmlspecialchars(checkInput($_POST,$film,'title',$_SESSION['formular'])); ?>"><br />
      <label for="tagline">Tagline</label><br />
      <input type="text" id="tagline" name="tagline" value="<?php echo htmlspecialchars(checkInput($_POST,$film,'tagline')); ?>"><br />
      <label for="original_language">Originalsprache</label><br />
      <input type="text" id="original_language" name="original_language" value="<?php echo htmlspecialchars(checkInput($_POST,$film,'original_language')); ?>"> <br />
      <label for="original_title">Originaltitel</label><br />
      <input type="text" id="original_title" name="original_title" value="<?php echo htmlspecialchars(checkInput($_POST,$film,'original_title')); ?>"> <br />
      <label for="release_date">Veröffentlichung*</label><br />
      <input type="text" id="release_date" name="release_date" value="<?php echo transformDate(checkInput($_POST,$film,'release_date')); ?>"> <br />
      <label for="runtime">Laufzeit (in Minuten)*</label><br />
      <input type="text" id="runtime" name="runtime" value="<?php echo htmlspecialchars(checkInput($_POST,$film,'runtime')); ?>"> <br />
      <label for="production_companies">Produktionsfirmen</label><br />
      <input type="text" id="production_companies" name="production_companies" value="<?php echo (!empty($_POST)) ? $_POST['production_companies'] : $production_companies; ?>"><br />
    </div>
    <div class='filmdetails'>
      <label for="genres">Genres*</label><br />
      <input type="text" id="genres" name="genres" value="<?php echo (!empty($_POST)) ? $_POST['genres'] : $genres; ?>"><br />
      <label for="budget">Produktionsbudget (in US-Dollar)</label><br />
      <input type="text" id="budget" name="budget" value="<?php echo (empty($film['budget'])) ? '' : number_format(floatval(checkInput($_POST,$film,'budget')),"2",",","."); ?>"> <br /> <?php // TODO: Ausgabe des Budget nach Submit checken ?>
      <label for="homepage">Website</label><br />
      <input type="text" id="homepage" name="homepage" value="<?php echo htmlspecialchars(checkInput($_POST,$film,'homepage'));  ?>">&nbsp;&nbsp;<?php echo (!empty($film['homepage'])) ? "<a href='{$film['homepage']}' target='_blank'>[Website öffnen]</a>" : '';?> <br />
      <label for="overview">Handlung*</label><br  />
      <textarea name='overview' id='overview' onkeydown="letter_counter()"><?php echo htmlspecialchars(checkInput($_POST,$film,'overview')); ?></textarea><br />
      <span id='letter'>Sie haben <span id='letter_counter'></span> Zeichen! Notwendig sind: <?php echo $OverviewMinZeichen; ?></span>
    </div>
    <?php
    if(!empty($film)){
     ?>
    <div class='filmtrailers'>
      <div>
        <h2>Trailers (nur Deutsch!)</h2>
      </div>
    <?php
      foreach($trailers['results'] as $trailer => $value){
        foreach($value as $item => $wert){
          if($item=='key'){
            $url = "https://www.youtube.com/embed/".$wert;
            echo "<div class='trailer'>";
            echo "<iframe width='260' height='175' src='{$url}'></iframe><br  /> ";
            echo "<label for='youtubekey'>YouTube Key</label><br  />";
            echo "<input type='text' id='youtubekey' name='youtubekey' value='{$wert}'>";
            echo "</div>";

          }
        }
      }
    ?>
    </div>
    <div class='cast'>
      <h3>Hauptdarsteller</h3><br />
      <?php
      $result = array_column($cast['cast'], 'name');
      $result2 = array_column($cast['cast'], 'character');
      $result3 = array_column($cast['cast'], 'profile_path');
      if(count($result) != 0){
        for ($i = 0; $i < 5; $i++) {
          if(!empty($result3[$i])){
            echo "<div>";
            echo "<img src='https://image.tmdb.org/t/p/w276_and_h350_face/".$result3[$i]."' width='200'/><br />";
            echo "<b>$result[$i]</b><br />";
            echo $result2[$i]."<br />";
            echo "</div>";
          }
        }
      }
      ?>
    </div>
    <div class='images'>
      <div>
        <h2>Bilder</h2>
      </div>
    <?php
      foreach($images['posters'] as $images => $value){
        foreach($value as $img => $wert){
          if($img=='file_path'){
            $url = "https://image.tmdb.org/t/p/w700_and_h392_bestv2".$wert;
            echo "<div class='img'>";
            echo "<img width='300' src='{$url}' /><br  /> ";
            echo "</div>";

          }
        }
      }
    ?>
    </div>
    <?php
  } //Trailers, Darsteller und BIlder sind nur für Filme von der MovieDB zulässig #// TODO: Diese Daten sollen für jede Filmanlage möglich sein

  ?>
</div>
<input type="submit" value="Film anlegen" />
<script>
    window.onload = function () {
           console.log('Dokument geladen');
           document.getElementById("letter_counter").innerHTML = '<?php echo $OverviewCount; ?>';
       }
    function letter_counter(){
      var minCount ='<?php echo $OverviewMinZeichen;?>';
      var count = document.getElementById("overview").value.length;
      document.getElementById("letter_counter").innerHTML = count;
        if(minCount < count){
          document.getElementById("letter").style.color="#008b45";
        } else {
          document.getElementById("letter").style.color="#000";
        }
    }
</script>
</form>

<?php
}
?>
