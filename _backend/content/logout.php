<?php
include_once '../inc/functions.inc.php';
sec_session_start();
// Alle $_SESSION Variablen entfernen
session_unset();
session_destroy();
 ?>
    <h1>Logout</h1>
    <p>
      Sie wurden erfolgreich ausgeloggt! <a href='index.php?p=login'>Login </a>
    </p>
