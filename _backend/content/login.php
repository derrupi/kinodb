<?php
include "../inc/functions.inc.php";
sec_session_start();

// wurde das Loginformular gesendet.
if(!empty($_POST)){
  $error = array();
  // Validierung der Eingaben
  if(empty($_POST['benutzername'])){
    $error[] = 'Ohne Benutzername können Sie sich nicht einloggen!';
  }

  if(empty($_POST['passwort'])){
    $error[] = 'Ohne Passwort können Sie sich nicht einloggen!';
  }

  if(empty($error)){
    if(!login($_POST['benutzername'],$_POST['passwort'])){
      $error[] = 'Benutzername oder Passwort ist falsch! Bitte versuchen Sie es erneut.';
    } else {
      // Letztes Login und Anzahl Logins in DB speichern
      query("UPDATE benutzer SET
        last_login = NOW(),
        anzahl_logins = anzahl_logins + 1
        WHERE id = '{$zeile["id"]}'
      ");

      // Umleitung in Admin-System
      header("Location: index.php?p=");
      exit;
    }
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>
      Loginbereich Fahrzeug-DB
    </title>
    <meta charset="utf-8" />
  </head>
  <body>
    <h1>Loginbereich Fahrzeug-DB</h1>

    <div>

      <?php
      if(!empty($error)){
        echo "<div><ul>";
        foreach ($error as $value) {
          echo "<li>";
          echo $value;
          echo "</li>";
        }
        echo "</ul></div>";
      }
      ?>

    </div>
    <form method="post" action="login.php" name="login">
      <div>
        <label for='benutzername'>Benutzername:</label>
        <input type="text" name='benutzername' id='benutzername' />
      </div>
      <div>
        <label for='passwort'>Passwort:</label>
        <input type='password' name='passwort' id='passwort' />
      </div>
      <div>
        <button type='submit'>Login</button>
      </div>
    </form>
  </body>
</html>
