####################################
### K I N O D B ####################
####################################

# Abschlussarbeit JWE 2018
Nachstehend werden die wichtigsten Funktionen von KinoDB beschrieben und der
Funktionsumfang erklärt.


Installation kann über composer erfolgen

```
composer create-project derrupi/kinodb
```

der aktuelle Datenbank-Dump befindet sich in ./install/kinodb.sql

Der Installationskript ist in der Beta-Phase => Verwendung auf eigene Gefahr.

## Eingesetzte Technologie
-  als Templateengine wurde Dwoo (http://dwoo.org/), weil es ein einfacher TemplateEngine ist und ggf. mit Smarty kompatibel ist. Jedoch ist Dwoo performanter wie Smarty
- PHPMailer zum versenden von eMails via SMTP

## Verwendete APIs
- The MovieDB
Damit Filme nicht immer manuell angelegt werden müssen, habe ich eine Schnittstelle zu TheMovieDB implementiert. Diese API ist kostenlos und sehr umfangreich dokumentiert. Alle Bilder und Informationen dürfen laut AGBs kostenlos eingebunden werden, im Gegenzug muss aber im Impressum auf TheMovieDB hingewiesen werden und das Logo muss eingebunden werden.
