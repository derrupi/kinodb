<h2>KinoDB Installer (BETA)</h2>
<form method="post" action="">
  <div>
    <label for="dbhost">Host:</label>
    <input type="text" name="dbhost" id="dbhost" value="localhost"/>
  </div>
  <div>
    <label for="dbuser">User:</label>
    <input type="text" name="dbuser" id="dbuser" value=""/>
  </div>
  <div>
    <label for="dbpasswort">Passwort:</label>
    <input type="password" name="dbpasswort" id="dbpasswort" value=""/>
  </div>
  <div>
    <label for="database">Database:</label>
    <input type="text" name="database" id="database" value=""/>
  </div>
  <hr />
  <div>
    <label for="adminvorname">Vorname:</label>
    <input type="text" name="adminvorname" id="adminvorname" value=""/>
  </div>
  <div>
    <label for="adminnachname">Nachname:</label>
    <input type="text" name="adminnachname" id="adminnachname" value=""/>
  </div>
  <div>
    <label for="adminemail">Admin Email:</label>
    <input type="text" name="adminemail" id="adminmail" value=""/>
  </div>
  <div>
    <label for="adminpasswort">Admin Passwort:</label>
    <input type="password" name="adminpasswort" id="adminpasswort" value=""/>
  </div>
  <hr />
  <div>
    <label for="ftpserver">FTP Server:</label>
    <input type="text" name="ftpserver" id="ftpserver" value=""/>
  </div>
  <div>
    <label for="ftpuser">FTP User:</label>
    <input type="text" name="ftpuser" id="ftpuser" value=""/>
  </div>
  <div>
    <label for="ftppasswort">FTP Passwort:</label>
    <input type="password" name="ftppasswort" id="ftppasswort" value=""/>
  </div>
  <div>
    <label for="ftproot">FTP Root:</label>
    <input type="text" name="ftproot" id="ftproot" value="/"/>
  </div>
  <div>
    <input type="submit" name="submit" value="Installer starten"/>
  </div>
</form>
<?php

if(!empty($_POST)){
  // Dieser Script wurde mit freundlicher Unterstützung des Internetes geklaut!

  $filename = 'kinodb.sql';
  $maxRuntime = 8; // less then your max script execution limit
  $dateinamen = "config-sample.php";

  $message = file_get_contents($dateinamen);
  $message = str_replace('%DBHOST%', $_POST['dbhost'], $message);
  $message = str_replace('%DBUSER%', $_POST['dbuser'], $message);
  $message = str_replace('%DBPASSWORT%', $_POST['dbpasswort'], $message);
  $message = str_replace('%DATABASE%', $_POST['database'], $message);
  $message = str_replace('%FTPSERVER%', $_POST['ftpserver'], $message);
  $message = str_replace('%FTPUSER%', $_POST['ftpuser'], $message);
  $message = str_replace('%FTPPASSWORT%', $_POST['ftppasswort'], $message);
  $message = str_replace('%FTPROOT%', $_POST['ftproot'], $message);

  // Schreiben des neuen Wertes
  $handle = fopen ("config.php", "w");
  if(fwrite ($handle, $message)){
    print ("config.php wurde geschrieben<br />");
  }
  fclose ($handle);

  $deadline = time()+$maxRuntime;
  $progressFilename = $filename.'_filepointer'; // tmp file for progress
  $errorFilename = $filename.'_error'; // tmp file for erro

  require_once "config.php";
  require_once "ftp.php";

  // Connect to the FTP
  $conn_id = chmod_open();

  // CHMOD each file and echo the results
  echo chmod_file($conn_id, 777, 'install/') ? 'CHMODed /install/ successfully!' : 'Error'."<br />";
  echo chmod_file($conn_id, 777, 'engine/') ? 'CHMODed /engine/ successfully!' : 'Error'."<br />";


  // Close the connection
  chmod_close($conn_id);

  $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD) OR die('connecting to host: '.DB_HOST.' failed: '.mysqli_error($link));

  $db_selected = mysqli_select_db($link, DB_DATABASE);

  if (!$db_selected) {
    // If we couldn't, then it either doesn't exist, or we can't see it.
    $sql = 'CREATE DATABASE '.DB_DATABASE;

    if (mysqli_query($link, $sql)) {
        print("Database ".DB_DATABASE." created successfully\n");
    } else {
        echo 'Error creating database: ' . mysqli_error($link) . "\n";
    }
  }

  mysqli_select_db($link, DB_DATABASE);
  ($fp = fopen($filename, 'r')) OR die('failed to open file:'.$filename);

  // check for previous error
  if( file_exists($errorFilename) ){
      die('<pre> previous error: '.file_get_contents($errorFilename));
  }

  // activate automatic reload in browser
  echo '<html><head> <meta http-equiv="refresh" content="'.($maxRuntime+2).'"><pre>';

  // go to previous file position
  $filePosition = 0;
  if( file_exists($progressFilename) ){
      $filePosition = file_get_contents($progressFilename);
      fseek($fp, $filePosition);
  }

  $queryCount = 0;
  $query = '';
  while( $deadline>time() AND ($line=fgets($fp, 1024000)) ){
      if(substr($line,0,2)=='--' OR trim($line)=='' ){
          continue;
      }

      $query .= $line;
      if( substr(trim($query),-1)==';' ){
          if( !mysqli_query($link, $query) ){
              $error = 'Error performing query \'<strong>' . $query . '\': ' . mysqli_error($link);
              file_put_contents($errorFilename, $error."\n");
              exit;
          }
          $query = '';
          file_put_contents($progressFilename, ftell($fp)); // save the current file position for
          $queryCount++;
      }
  }

  if( feof($fp) ){
      print ('dump successfully restored!<br />');

          $ausgabe = array(
            'vorname' => $_POST['adminvorname'],
            'nachname' => $_POST['adminnachname'],
            'passwort' => password_hash($_POST['adminpasswort'], PASSWORD_DEFAULT),
            'email' => $_POST['adminemail']
          );

          $max = count($ausgabe);
          $i = 1;

          $sql = "INSERT INTO benutzer SET ";
          foreach($ausgabe as $key => $item){
            $sql .= $key." = '".$item."'";
            if ($i++ != $max) {
              $sql .= ", ";
            } else {
              $sql .= " ";
            }
          }

        if(mysqli_query($link, $sql) ){
          print("Adminbenutzer wurde erfolgreich angelegt!<br />");
        }
        if(!file_exists($_SERVER["DOCUMENT_ROOT"]."/engine/config.php")){
        //copy() - Datei kopieren
        if (!copy($_SERVER["DOCUMENT_ROOT"]."/install/config.php",
          $_SERVER["DOCUMENT_ROOT"]."/engine/config.php")) {
           print ("failed to copy $file...<br>\n");
        }
        else{
           //unlink() - Datei löschen
           if(unlink($_SERVER["DOCUMENT_ROOT"]."/install/config.php") && unlink($_SERVER["DOCUMENT_ROOT"]."/install/kinodb.sql")){
             print ("config.php wurde kopiert und gelöscht.<br />");
             print ("Bitte das Verzeichnis /install/ samt Inhalt löschen.<br />");

           }
        }
      }
    }
}
