
--
-- Datenbank: `kinodb`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `benutzer`
--

CREATE TABLE `benutzer` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin` int(1) UNSIGNED DEFAULT NULL,
  `benutzername` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vorname` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nachname` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passwort` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `anzahl_logins` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `benutzer_passwort`
--

CREATE TABLE `benutzer_passwort` (
  `id` int(190) UNSIGNED NOT NULL,
  `user` int(190) UNSIGNED NOT NULL,
  `code` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `timestamp` int(190) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `filme`
--

CREATE TABLE `filme` (
  `id` int(190) UNSIGNED NOT NULL,
  `moviedb_id` int(15) NOT NULL,
  `poster_path` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tagline` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `original_language` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `original_title` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `release_date` date NOT NULL,
  `runtime` int(3) NOT NULL,
  `production_companies` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `budget` int(20) NOT NULL,
  `homepage` varchar(260) COLLATE utf8mb4_unicode_ci NOT NULL,
  `overview` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `filme_genres`
--

CREATE TABLE `filme_genres` (
  `id` int(190) UNSIGNED NOT NULL,
  `film` int(190) UNSIGNED NOT NULL,
  `genre` int(190) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `genres`
--

CREATE TABLE `genres` (
  `id` int(190) UNSIGNED NOT NULL,
  `moviedb_id` int(190) NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kinosaele`
--

CREATE TABLE `kinosaele` (
  `id` int(10) NOT NULL,
  `saalnr` int(5) NOT NULL,
  `sitzplaetze` int(10) NOT NULL,
  `reihen` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `laufzeit`
--

CREATE TABLE `laufzeit` (
  `id` int(190) UNSIGNED NOT NULL,
  `film` int(190) UNSIGNED NOT NULL,
  `startdatum` date NOT NULL,
  `enddatum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `login_attempts`
--

CREATE TABLE `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `reservierungen`
--

CREATE TABLE `reservierungen` (
  `id` int(190) UNSIGNED NOT NULL,
  `kunde` int(10) UNSIGNED NOT NULL,
  `vorstellung` int(190) UNSIGNED NOT NULL,
  `reihe` int(190) UNSIGNED NOT NULL,
  `sitzplatz` int(190) UNSIGNED NOT NULL,
  `reservierungsnummer` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `settings`
--

CREATE TABLE `settings` (
  `id` int(1) UNSIGNED NOT NULL,
  `grundpreis` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `settings`
--

INSERT INTO `settings` (`id`, `grundpreis`) VALUES
(1, '9,70');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `sitzplaetze`
--

CREATE TABLE `sitzplaetze` (
  `id` int(140) NOT NULL,
  `kinosaal` int(10) NOT NULL,
  `reihe` int(50) NOT NULL,
  `sitzplatz` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `vorstellungen`
--

CREATE TABLE `vorstellungen` (
  `id` int(148) UNSIGNED NOT NULL,
  `film` int(190) UNSIGNED NOT NULL,
  `kinosaal` int(148) NOT NULL,
  `datum` date NOT NULL,
  `uhrzeit` time NOT NULL,
  `input` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `benutzer`
--
ALTER TABLE `benutzer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indizes für die Tabelle `benutzer_passwort`
--
ALTER TABLE `benutzer_passwort`
  ADD PRIMARY KEY (`id`),
  ADD KEY `benutzer_passwort_ibfk_1` (`user`);

--
-- Indizes für die Tabelle `filme`
--
ALTER TABLE `filme`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `filme_genres`
--
ALTER TABLE `filme_genres`
  ADD PRIMARY KEY (`id`),
  ADD KEY `film` (`film`),
  ADD KEY `genre` (`genre`);

--
-- Indizes für die Tabelle `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `kinosaele`
--
ALTER TABLE `kinosaele`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `laufzeit`
--
ALTER TABLE `laufzeit`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `film` (`film`);

--
-- Indizes für die Tabelle `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD KEY `user_id` (`user_id`);

--
-- Indizes für die Tabelle `reservierungen`
--
ALTER TABLE `reservierungen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kunde` (`kunde`),
  ADD KEY `reservierungen_ibfk_1` (`vorstellung`);

--
-- Indizes für die Tabelle `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `sitzplaetze`
--
ALTER TABLE `sitzplaetze`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kinosaal` (`kinosaal`);

--
-- Indizes für die Tabelle `vorstellungen`
--
ALTER TABLE `vorstellungen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vorstellungen_ibfk_2` (`kinosaal`),
  ADD KEY `vorstellungen_ibfk_1` (`film`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `benutzer`
--
ALTER TABLE `benutzer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT für Tabelle `benutzer_passwort`
--
ALTER TABLE `benutzer_passwort`
  MODIFY `id` int(190) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT für Tabelle `filme`
--
ALTER TABLE `filme`
  MODIFY `id` int(190) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT für Tabelle `filme_genres`
--
ALTER TABLE `filme_genres`
  MODIFY `id` int(190) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT für Tabelle `genres`
--
ALTER TABLE `genres`
  MODIFY `id` int(190) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT für Tabelle `kinosaele`
--
ALTER TABLE `kinosaele`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT für Tabelle `laufzeit`
--
ALTER TABLE `laufzeit`
  MODIFY `id` int(190) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT für Tabelle `reservierungen`
--
ALTER TABLE `reservierungen`
  MODIFY `id` int(190) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT für Tabelle `sitzplaetze`
--
ALTER TABLE `sitzplaetze`
  MODIFY `id` int(140) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT für Tabelle `vorstellungen`
--
ALTER TABLE `vorstellungen`
  MODIFY `id` int(148) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=352;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `benutzer_passwort`
--
ALTER TABLE `benutzer_passwort`
  ADD CONSTRAINT `benutzer_passwort_ibfk_1` FOREIGN KEY (`user`) REFERENCES `benutzer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `filme_genres`
--
ALTER TABLE `filme_genres`
  ADD CONSTRAINT `filme_genres_ibfk_1` FOREIGN KEY (`film`) REFERENCES `filme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `filme_genres_ibfk_2` FOREIGN KEY (`genre`) REFERENCES `genres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `laufzeit`
--
ALTER TABLE `laufzeit`
  ADD CONSTRAINT `laufzeit_ibfk_1` FOREIGN KEY (`film`) REFERENCES `filme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `reservierungen`
--
ALTER TABLE `reservierungen`
  ADD CONSTRAINT `reservierungen_ibfk_1` FOREIGN KEY (`vorstellung`) REFERENCES `vorstellungen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservierungen_ibfk_2` FOREIGN KEY (`kunde`) REFERENCES `benutzer` (`id`);

--
-- Constraints der Tabelle `sitzplaetze`
--
ALTER TABLE `sitzplaetze`
  ADD CONSTRAINT `sitzplaetze_ibfk_1` FOREIGN KEY (`kinosaal`) REFERENCES `kinosaele` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `vorstellungen`
--
ALTER TABLE `vorstellungen`
  ADD CONSTRAINT `vorstellungen_ibfk_1` FOREIGN KEY (`film`) REFERENCES `filme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vorstellungen_ibfk_2` FOREIGN KEY (`kinosaal`) REFERENCES `kinosaele` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;
