<?php
// define Root
define('DIR', "/kinodb/");

// define mySQL Configuration
define('DB_HOST', '%DBHOST%');
define('DB_USER', '%DBUSER%');
define('DB_PASSWORD', '%DBPASSWORT%');
define('DB_DATABASE', '%DATABASE%');

//define FTP Connection
define('FTP_SERVER', '%FTPSERVER%');
define('FTP_USER', '%FTPUSER%');
define('FTP_PASSWORD', '%FTPPASSWORT%');
define('FTP_ROOT', '%FTPROOT%');

// MovieDB Api Key
define('MOVIEDB_APIKEY','af5d92b0fd6b1c75829cc368a3c513fe')

define("SECURE", FALSE);    // NUR FÜR DIE ENTWICKLUNG!!!!
