<?php

function chmod_open()
{
    // Use your own FTP info
    $ftp_user_name = FTP_USER;
    $ftp_user_pass = FTP_PASSWORD;
    $ftp_root = FTP_ROOT;
    $ftp_server = __DIR__;
    $conn_id = ftp_connect($ftp_server);
    $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
    return $conn_id;
}

function chmod_file($conn_id, $permissions, $path)
{
    if (ftp_site($conn_id, 'CHMOD ' . $permissions . ' ' . $ftp_root . $path) !== false)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

function chmod_close($conn_id)
{
    ftp_close($conn_id);
}

// CHMOD the required setup files


?>
