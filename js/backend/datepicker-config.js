
  $(function() {
    var dateFormat = "dd.mm.yy";
    $("#fromdate").datepicker({
      dateFormat: "dd.mm.yy",
      firstDay: 1,
      minDate: 1,
      showButtonPanel: true,
      closeText: 'Kalender schließen',
      currentText: 'Heute',
      dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
      dayNamesMin: ['SO', 'MO', 'DI', 'MI', 'DO', 'FR', 'SA'],
      monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai',
        'Juni', 'Juli', 'August', 'September', 'Oktober',  'November', 'Dezember'],
      showAnim: 'blind'
    });

    $("#fromdate").on( "change", function() {
      $("#todate").datepicker( "option", "minDate", getDate( this ) );
      $( "#todate" ).datepicker( "option", "defaultDate", +21 );
    })

    $("#todate").datepicker({
      dateFormat: "dd.mm.yy",
      showButtonPanel: true,
      closeText: 'Kalender schließen',
      currentText: 'Heute',
      dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
      dayNamesMin: ['SO', 'MO', 'DI', 'MI', 'DO', 'FR', 'SA'],
      monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai',
        'Juni', 'Juli', 'August', 'September', 'Oktober',  'November', 'Dezember'],
      showAnim: 'blind'
    });

    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }

      return date;
    }
} );
