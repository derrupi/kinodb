<?php

require_once 'api.inc.php';
require_once '../engine/db/datenbank.inc.php';
require_once '../engine/film/filme.inc.php';
require_once '../engine/config.php';

$api = new api();

if(!empty($_GET['token'])){
  $api->setToken($_GET['token']);
}

if(!empty($_GET['methode'])){
  $api->setMethode($_GET['methode']);
}

if(!empty($_GET['movie'])) {
  $api->setMovieId($_GET['movie']);
}

if(array_key_exists("suche", $_GET)){
  $api->setSuche($_GET["suche"]);
}

$api->ausfuehren();
