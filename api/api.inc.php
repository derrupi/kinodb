<?php

class api {

 private $methode = null;
 private $suche = null;
 private $authCode = 'LN6Chh7qEPULmLTqD4GpKtAy9FPksv';
 private $token = null;
 private $movieID;

 public function setMethode($methode)
 {
   $this->methode = $methode;
 }

 public function setSuche($suche){
   $this->suche = $suche;
 }

 public function setToken($token){
   $this->token = $token;
 }

 public function setMovieId($movie){
   $this->movieID = $movie;
 }

 public function ausfuehren(){
   if($this->checkToken()){
    if(empty($this->methode)){
      $ausgabe = $this->error("0", "Du musst eine Methode angeben");
    } else {
      if($this->methode == "getAllMovies"){
         $ausgabe["movie"] = $this->getAllMovies();
      } else if($this->methode == "getMovieDetail"){
          $ausgabe["movie"] = $this->getMovieDetail();
      } else {
        $ausgabe[] = $this->error("0", "Die Methode {$this->methode} ist ungültig");
      }

    }


  } else {
    $ausgabe = $this->error("0", "Anmeldung war nicht erfolgreich");
  }
  echo json_encode($ausgabe);
 }

 private function checkToken(){
   if(!empty($this->token)){
     if($this->token != $this->authCode){
       return false;
     } else {
       return true;
     }
   } else {
     return false;
   }
 }

private function getAllMovies(){
  $db = db_datenbank::get_instanz();

  $sql = "SELECT f.*, v.film, v.datum FROM filme f
    INNER JOIN vorstellungen v ON v.film = f.id AND v.datum = CURDATE()
    GROUP BY f.id";
  $ergebnis = $db->query($sql);

  $ausgabe = array();
  while($row = mysqli_fetch_assoc($ergebnis)){
    $ausgabe[] = array(
      'id' => $row['id'],
      'title' => $row['title'],
      'poster' => $row['poster_path'],
      'tagline' => $row['tagline'],
      'overview' => $row['overview'],
      'vorstellungen' => $this->getShow($row['id'])
    );
  }
  return $ausgabe;
}

public function getShow($id, $datum = ''){
  $db = db_datenbank::get_instanz();
  $id = $db->escape($id);

  $sql = "SELECT v.*, k.saalnr FROM vorstellungen v, kinosaele k
  WHERE v.film = '{$id}'
  AND k.id = v.kinosaal";
  if(!empty($datum)){
    $sql .= " AND datum = '{$datum}'";
  } else {
    $datum = date("Y-m-d");
    $sql .= " AND datum = '{$datum}'";
  }
  $arr = $db->query($sql);


  $ergebnis = array();

    while($row = mysqli_fetch_assoc($arr)){
      $ergebnis[] = $row;
      // $ergebnis['seats'] = film_tickets::countReservations('156');
    }
    return $ergebnis;
}

private function getMovieDetail(){
  $db = db_datenbank::get_instanz();
  $this->movieID = $db->escape($this->movieID);

  $sql = "SELECT * FROM filme WHERE id = '".$this->movieID."'";
  $ergebnis = $db->query($sql);
  $ausgabe = array();

  $count = mysqli_num_rows($ergebnis);
  if($count >= 1){
    while($row = mysqli_fetch_assoc($ergebnis)){
      $ausgabe[] = array(
        'id' => $row['id'],
        'title' => $row['title'],
        'poster' => $row['poster_path'],
        'tagline' => $row['tagline'],
        'overview' => $row['overview'],
        'vorstellungen' => $this->getShow($row['id'])
      );
    }
  } else {
    $ausgabe[] = $this->error("0","Es konnte kein Film mit der ID {$this->movieID} gefunden werden");
  }

  return $ausgabe;
}

private function error($status = 0, $fehlermeldung){
   return array(
     "status" => $status,
     "error" => $fehlermeldung
   );
 }

}
